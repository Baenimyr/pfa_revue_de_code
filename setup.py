#!/usr/bin/env python3

from distutils.core import setup

setup(name='coderev', version='0.2', packages=['libcoderev', "libcoderev.command"], scripts=["coderev"],
	requires=["unidiff (>=0.5, <0.6)", "ddt (>=1.4)", "toml (>=0.10, <0.11)"],
license="GNU 3",
	description="A non-intrusive way to comment files and use version manager.")
