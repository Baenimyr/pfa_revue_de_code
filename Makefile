##
# Coderev
##

PY=python3

.PHONY: tests

tests:
	@make --no-print-directory -C test tests

setup:
	@python3 -m pip install -r requirements.txt

install:
	@pip3 install .

uninstall:
	@pip3 uninstall -y coderev
