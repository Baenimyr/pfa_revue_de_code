import unidiff
import difflib as d

def generate_diff(old_filename,new_filename):
    "Generate a diff string in the unified format"
    old_content = open(old_filename, "r").readlines()
    new_content = open(new_filename, "r").readlines()
    unified_diff = d.unified_diff(
        old_content, new_content, old_filename, new_filename
    )
    return "".join(list(unified_diff))

def inside(line, hunk) -> bool:
    """Return whether `line` is inside `hunk`
    :rtype: bool
    """
    return (int(line) >= hunk.source_start) and (
        int(line) < (hunk.source_start + hunk.source_length)
    )


def line_move_no_change(before,after,anchors):
    patch_set = unidiff.PatchSet(generate_diff(old,new))
    new_anchors = []
    anchor_index = 0
    print(patch_set)
    for anchor_line in anchors:
        new_anchors.append(anchor_line)
        for patch in patch_set:
            for hunk in patch:
                #hunk.source) #--> lignes avec '-'  et lignes de contexte old
                              #on retrouve donc entièrement le contenu de old (mais avec marqueurs '-')
                #hunk.target  #--> lignes avec '+' et lignes de context dans new
                              #on retrouve donc entièrement le contenu de new (mais avec marqueurs '+')
                removed_lines=[]
                added_lines=[]
                line_number = 0
                for line in hunk:
                    line_number = line_number+1 #need to keep track of current line in hunk
                    if line.source_line_no != None and line.target_line_no != None and line.source_line_no == anchor_line:
                        #the line has not been added or removed
                        new_anchors[anchor_index]=line.target_line_no
                    elif line.source_line_no != None and line.target_line_no == None and line.source_line_no == anchor_line:
                        #the line has been removed from source
                        removed_lines.append(line)
                    elif line.source_line_no == None and line.target_line_no != None:
                        #the line has been added in target
                        added_lines.append(line)
                #compare added lines and removed lines by content and update anchors accordingly
                for r in removed_lines:
                    for a in added_lines:
                        lr = str(r)
                        la = str(a)
                        if lr[1::] == la[1::] and r.source_line_no == anchor_line:
                            new_anchors[anchor_index]=a.target_line_no

        anchor_index = anchor_index+1                    
    lb = open(before, "r").readlines()
    la = open(after, "r").readlines()
    i = 0
    for l in lb:
        i = i+1
        print(str(i) + " " + l[:-1])
    print("\n")
    i = 0
    for l in la:
        i = i+1
        print(str(i) + " " + l[:-1])
    print("\n")
    print("Original anchors : ")
    print(anchors)
    print("Bumped anchors : ")
    return new_anchors


old="./old.txt"
new="./new.txt"

anchor_list=[1,2,3]



print(line_move_no_change(old,new,anchor_list))
