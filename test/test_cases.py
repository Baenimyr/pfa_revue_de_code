#!/usr/bin/env python3
import csv
import io
import logging
import sys
import unittest
from os.path import dirname

from ddt import data, ddt

sys.path.append(dirname(dirname(__file__)))
from libcoderev import *
from libcoderev.updater import AnchorUpdater
from libcoderev.versionning import VersionProvider


class Transition(NamedTuple):
	source_line: Tuple[int, int]
	source_offset: Tuple[int, int]
	target_line: Tuple[int, int]
	target_offset: Tuple[int, int]


class Test(NamedTuple):
	name: str
	content_before: str
	content_after: str
	results: List[Transition]
	
	def __repr__(self):
		return self.name


class TestVersionProvider(VersionProvider):
	def __init__(self, test: Test):
		super(TestVersionProvider, self).__init__()
		self.test = test
	
	def actual_version(self) -> str:
		return "after"
	
	def previous_versions(self, commit: str = "HEAD") -> List[str]:
		return ["before"] if commit in ["HEAD", "after"] else list()
	
	def diff(self, a: str, b: str) -> TextIO:
		if a == "before" and b == "after":
			return io.StringIO("".join(difflib.unified_diff(self.test.content_before.splitlines(True),
				self.test.content_after.splitlines(True), "a/" + self.test.name, "b/" + self.test.name)))
	
	def file_content(self, version: str, filename: str):
		if version == "before":
			return io.StringIO(self.test.content_before)
		elif version == "after":
			return io.StringIO(self.test.content_after)


def _read_anchor_pos(text: str) -> Tuple[int, Optional[int]]:
	i = text.find(":")
	if i > 0:
		return int(text[:i]) - 1, int(text[i + 1:])
	else:
		return int(text) - 1, None


def generate_cases(resources_path: str, sort=True):
	"""
		Returns a sorted list (by case number) of cases corresponding to each test case in provided directory path
	"""
	cases = []
	for case in listdir(join(resources_path, "before")):
		cases.append(generate_case(resources_path, case))
	if sort is True:
		cases.sort()
	return cases


def generate_case(resources_path: str, case_name: str) -> Test:
	with open(join(resources_path, "expectations", case_name), "r") as expectation, open(
			join(resources_path, "before", case_name), "r") as src, open(join(resources_path, "after", case_name),
		"r") as tg:
		src_content = src.readlines()
		tg_content = tg.readlines()
		results = list()
		for row in csv.reader(expectation, delimiter=" "):
			if len(row) == 4:
				s0 = _read_anchor_pos(row[0])
				s1 = _read_anchor_pos(row[1])
				e0 = _read_anchor_pos(row[2])
				e1 = _read_anchor_pos(row[3])
				
				results.append(Transition((s0[0], s1[0]),
					(s0[1] if s0[1] else 0, s1[1] if s1[1] else len(src_content[s1[0]]) - 1) if s1[0] >= 0 else 0,
					(e0[0], e1[0]),
					(e0[1] if e0[1] else 0, e1[1] if e1[1] else len(tg_content[e1[0]]) - 1) if e1[0] >= 0 else 0))
		return Test(case_name, "".join(src_content), "".join(tg_content), results)


@ddt
class TestDiff(unittest.TestCase):
	results = []
	
	@unittest.skip
	@data(*generate_cases(join(dirname(__file__), "ressources")))
	def test_cases(self, case):
		self.simple_test(case)
		
	def test_01(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "01_line_inversion.txt"))
		
	def test_02(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "02_two_lines_grouped_in_one.txt"))
		
	def test_03(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "03_inversion_if_and_else.txt"))
		
	def test_04(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "04_every_instruction_put_on_the_same_line.txt"))
		
	def test_05(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "05_spreading_a_command_on_multiple_lines.txt"))
		
	def test_06(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "06_bloc_inversion.txt"))
		
	def test_07(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "07_extracting_code_in_a_function.txt"))
		
	def test_09(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "09_spliting_a_for_into_a_wwhile.txt"))
		
	def test_10(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "10_factorize_code_similar_in_two_functions_by_creating_a_new_one.txt"))
		
	def test_11(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "11_refactoring_multiple_lines_into_one__anchor.py"))
		
	def test_12(self):
		self.simple_test(generate_case(join(dirname(__file__), "ressources"), "12_minor_modification_to_line__Makefile.txt"))
	
	def simple_test(self, case: Test):
		log = logging.getLogger(f"test.{case.name}")
		log.setLevel(logging.INFO)
		anchor_manager = AnchorManager()
		annotations_manager = AnnotationManager()
		anchor_ids = list()
		# get all the anchors for this case
		for T in case.results:
			anchor = Anchor("before", case.name, T.source_line, T.source_offset)
			anchor_ids.append(anchor_manager.register_anchor(anchor))
		
		version_provider = TestVersionProvider(case)
		updater = AnchorUpdater(anchor_manager, annotations_manager, version_provider,
			should_save=False)
		updater.options["simple"] = True
		updater.options["slip"] = True
		updater.options["leven"] = True
		updater.options["filter.lonely"] = True
		updater.update(("before", "after"), auto=2)
		
		success_rate = 0
		generated_anchors = list(map(anchor_manager.get_by_id, anchor_manager.anchors_by_version("after")))
		log.debug("anchors:\n" + "\n".join(map(repr, generated_anchors)))
		for anchor in generated_anchors:
			if anchor.get_state() == AnchorState.ERROR:
				self.assertTrue(any(
					anchor.lines == T.source_line and anchor.line_offsets == T.source_offset for T in case.results if
						T.target_line == (-2, -2)), f"{anchor} must not be in error state.")
			else:
				self.assertTrue(
					any(anchor.lines == T.target_line and anchor.line_offsets == T.target_offset for T in case.results),
					f"{anchor} has no place in this world.")
		
		for T in case.results:
			if T.target_line[0] >= 0 and T.target_line[1] >= 0:
				self.assertTrue(any(anchor.lines == T.target_line and anchor.line_offsets == T.target_offset for anchor in
					generated_anchors), f"{T} has no corresponding anchor.")
		
		TestDiff.results.append((case.name, len(case.results)))
	
	# print("\nSuccess rate for {} : {}".format(case.caseName,success_rate/len(expected)))
	
	@classmethod
	def tearDownClass(cls):
		print("\n")
		for test, res in cls.results:
			print(f"| {test:<80} | {res:3d} OK |")


if __name__ == "__main__":
	unittest.main()
