ORIGINAL FILE
1 int a = 1;
2
3 if ( a == 0){return 0;}else{return 1;}

ALTERED FILE
1 int a = 1;
2
3 if ( a == 0){
4   return 0;
5 }else{
6   return 1;
7 }

With the simple strategie we observe that :
Success rate : 1.0 with strategies ['simple']
Anchor on line 1 was expected on 1 and was bumped on 1
Anchor on line 2 was expected on 2 and was bumped on 2
Anchor on line 3 was expected on 3 and was bumped on 3

With the leven strategie we observe that :



Other observation :
