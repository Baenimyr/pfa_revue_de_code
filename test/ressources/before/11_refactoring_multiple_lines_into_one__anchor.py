# coding: utf-8
# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

"""
The main goal of this module is to manipulate the anchors.
Anchors are used to locate a precise part of the code ( a given line ).
"""

from enum import Enum
from typing import List, Optional, Tuple


class AnchorState(Enum):
    """ Differents states of an anchor.
    CONFIRMED means the user placed it or the system is 100% sure.
    AMBIGUOUS after an automatic update but the system is not sure. The anchor should be at the expected position.
    ERROR after an automatic update which failed to reposition the anchor.
    The anchor should be at its old known position.
    """

    CONFIRMED = "OK"
    AMBIGUOUS = "Ambiguous"
    ERROR = "Error"

    def __gt__(self, other):
        if self == AnchorState.CONFIRMED:
            return other == AnchorState.AMBIGUOUS or other == AnchorState.ERROR
        elif self == AnchorState.AMBIGUOUS:
            return other == AnchorState.ERROR
        else:
            return False


class Anchor:
    """ An anchor.
    An anchor can be associated with only one file, but can contains multiples lines of it.
    """

    def __init__(
        self,
        anchor_id: int,
        filename: str,
        lines: Tuple[int, int],
        offsets: Tuple[int, int] = (0, 0),
        data=None,
    ):
        if any(l < 0 for l in lines):
            raise ValueError(
                "The line numbers must be 2 positive digits not '" + str(lines) + "'"
            )
        if offsets is None or (
            isinstance(offsets, (tuple, list))
            and (
                len(offsets) != 2
                or offsets[0] < 0
                or offsets[1] < 0
                or offsets[0] > offsets[1]
            )
        ):
            raise ValueError(
                "The offset must be a tuple of 2 positive digits in ascending order not '"
                + str(offsets)
                + "'"
            )

        self.anchor_id: int = anchor_id
        self._state = AnchorState.CONFIRMED
        self.filename: str = filename
        self.lines: Tuple[int, int] = (lines, lines) if isinstance(
            lines, int
        ) else tuple(lines)
        self.line_offsets: Tuple[int, int] = (offsets, 0) if isinstance(
            offsets, int
        ) else tuple(offsets)
        self.data = data

    def change_filename(self, new_file: str):
        self.filename = new_file

    def change_position(self, lines: Tuple[int, int], offset: Tuple[int, int]):
        self.lines = lines
        self.line_offsets = offset

    def set_state(self, state: AnchorState):
        """ Change the current state of the anchor.
        Allow the system or the user to confirm an ambiguous anchor.
        During an automatic update of the anchor, the state can become AMBIGUOUS or ERROR.
        """
        self._state = state

    def get_state(self) -> AnchorState:
        """ Return the actual state of this anchor. """
        return self._state

    def __eq__(self, other):
        if isinstance(other, Anchor):
            return (
                self.filename == other.filename
                and self.lines[0] == other.lines[0]
                and self.lines[1] == other.lines[1]
                and self.line_offsets[0] == other.line_offsets[0]
                and self.line_offsets[1] == other.line_offsets[1]
            )

    def __repr__(self):
        return "Anchor {} ({}) in {:s} at ({:d}:{:d}, {:d}:{:d})\t{}".format(
            self.anchor_id,
            self.get_state(),
            self.filename,
            self.lines[0],
            self.line_offsets[0],
            self.lines[1],
            self.line_offsets[1],
            "" if self.data is None else self.data,
        )

    def __str__(self):
        return repr(self)

    def __hash__(self):
        return (
            hash(self.filename) * 31 * 31
            + hash(self.lines) * 31
            + hash(self.line_offsets)
        )

    def __copy__(self):
        anchor = Anchor(self.anchor_id, self.filename, self.lines, self.line_offsets)
        anchor._state = self._state
        anchor.data = self.data
        return anchor


class AnchorManager:
    def __init__(self):
        self.anchors: List[Anchor] = []

    def next_id(self) -> int:
        return len(self.anchors) + 1

    def get_by_id(self, id: int) -> Optional[Anchor]:
        """
        Return the anchor with the correct identifier.

        :param id: the id of the anchor.
        :return: the anchor object
        """

        for anchor in self.anchors:
            if anchor.anchor_id == id:
                return anchor
        return None

    def __contains__(self, item: int) -> bool:
        return item in (a.anchor_id for a in self.anchors)

    def add(
        self,
        filename,
        lines: Tuple[int, int],
        offsets: Tuple[int, int] = (0, 0),
        data=None,
    ):
        """ write in the shared file the reference id, the path of the file and the line number.
        :return: the new anchor id
        """
        identifier = self.next_id()
        self.anchors.append(
            Anchor(identifier, filename, lines=lines, offsets=offsets, data=data)
        )
        return identifier

    def add_anchor(self, anchor: Anchor) -> int:
        identifier = self.next_id()
        copy = anchor.__copy__()
        copy.anchor_id = identifier
        self.anchors.append(copy)
        return identifier

    def remove(self, id_ref: int) -> None:
        """delete the anchor with the id that matches id_ref"""
        pass

    def all_in_file(self, filename: str):
        return [anchor for anchor in self.anchors if anchor.filename == filename]

    def cat_anchors(self, labeled_anchors, filename):
        if filename:
            with open(filename, "r") as file:
                for line_number, file_line in enumerate(file):
                    print_line(line_number, file_line, labeled_anchors)


def anchor_is_at_line(anchor, line):
    first_line, last_line = anchor.lines
    return first_line <= line <= last_line


def print_line(line_number, file_line, labeled_anchors):

    labels_of_concerned_anchors = [
        label
        for anchor, label in labeled_anchors.items()
        if anchor.lines[0] == line_number
    ]

    boxes_of_anchors = [
        box_char(anchor.lines, line_number) for anchor, label in labeled_anchors.items()
    ]

    line_desc = ""
    line_desc = ", ".join(map(str, labels_of_concerned_anchors))
    box_chars = "".join(boxes_of_anchors)

    print(
        "{:20} {:{anchors_size}} {}: {}".format(
            line_desc,
            box_chars,
            line_number,
            file_line,
            anchors_size=len(labeled_anchors),
        ),
        end="",
    )


def box_char(lines, line):
    if lines[0] == lines[1] == line:
        return "["
    elif lines[0] == line:
        return "┌"
    elif lines[1] == line:
        return "└"
    elif lines[0] <= line <= lines[1]:
        return "│"
    return " "
