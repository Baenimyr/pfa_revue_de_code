#!/bin/bash
#For each test case directory, produces the git diff file using testNUMBER_before and testNUMBER_after txt files.



for n in $(seq 1 10)
do
git diff --no-index "test$n/test$n""_before.txt" "test$n/test$n""_after.txt" > "test$n/test$n""_diff.txt"
done

