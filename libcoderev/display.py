# coding:utf8
from typing import *

from .anchor import Anchor, AnchorManager
from .annotation import AnnotationManager
from .versionning import VersionProvider

_DESCRIPTION = 30


def print_line(line_number: int, file_line: str, anchors: Iterable[Anchor], labels: List[str], cursor: Tuple[int, int]):
	""" Print the line `line_number` with the correct format. """
	
	boxes_of_anchors = map(lambda anchor: box_char(anchor.lines, line_number), anchors)
	line_desc = labels[0] if len(labels) > 0 else ""
	if len(line_desc) > _DESCRIPTION:
		line_desc = line_desc[:_DESCRIPTION - 3] + "..."
	box_chars = "".join(boxes_of_anchors)
	
	file_line = file_line[:-1]
		
	if cursor:
		# Light gray background
		file_line = f"{file_line[:cursor[0]]}\033[47;30m{file_line[cursor[0]:cursor[1]]}\033[0m{file_line[cursor[1]:]}"
	
	print(f"{line_desc:<{_DESCRIPTION}} {box_chars:>5} {line_number + 1}: {file_line}")
	
	for i in range(1, len(labels)):
		print(labels[i])


def box_char(lines: Tuple[int, int], line: int) -> str:
	""" Computes the chars used to represent the anchor.
	`lines` is a tuple of boundary lines for an anchor.
	"""
	if lines[0] == lines[1] == line:
		return "["
	elif lines[0] == line:
		return "┌"
	elif lines[1] == line:
		return "└"
	elif lines[0] <= line <= lines[1]:
		return "│"
	return " "


def cat_anchors(anchors: List[Anchor], labels: Dict[int, List[str]], filename: str, version_provider, version,
		context: int):
	""" Display the content of the file `filename` side by side with
	information about anchors present in the file.
	:param anchors: list of anchors to show
	:param filename: name of the file to display
	:param version_provider: tool which can give the file content
	:param version: version key
	:param context: limit the display to zones of n lines around anchors.
	"""
	if filename:
		context_lines = set()
		if context > 0:
			for anchor in anchors:
				context_lines.update(anchor.context_lines(context))
		with version_provider.file_content(version, filename) as file:
			for line_number, file_line in enumerate(file):
				if context == 0 or line_number in context_lines:
					anchors_at_line = list(filter(lambda anchor: anchor.is_at_line(line_number), anchors))
					if len(anchors_at_line) > 0:
						start_cursor = min(
							map(lambda a: a.line_offsets[0] if a.lines[0] == line_number else 0, anchors_at_line))
						end_cursor = max(
							map(lambda a: a.line_offsets[1] if a.lines[1] == line_number else len(file_line),
								anchors_at_line))
						cursors = (start_cursor, end_cursor)
					else:
						cursors = None
					
					print_line(line_number, file_line, filter(lambda a: a.is_at_line(line_number), anchors),
						labels.get(line_number, list()), cursors)


def cat_annotations(annotations_manager: AnnotationManager, anchors_manager: AnchorManager,
		version_manager: VersionProvider, version: str, filename: str, context: int = 0):
	"""
	Display annotations present in filename.
	:param annotations_manager: provides data about the annotations
	:param anchors_manager: provides data about the anchors
	:param version_manager: allow displaying an older state of the file
	:param version: version to load and display
	:param filename: file to display
	:param context: limit to the strict minimum
	"""
	anchors_manager.load_anchors(version)
	anchors_in_file = anchors_manager.all_in_file(filename)
	labels = dict()
	anchors = list()
	for anchor_sha in anchors_in_file:
		anchor = anchors_manager.get_by_id(anchor_sha)
		anchors.append(anchor)
		for ann_sha in annotations_manager.get_annotations_for_anchor(anchor_sha):
			if anchor.lines[0] not in labels:
				labels[anchor.lines[0]] = list()
			labels[anchor.lines[0]].append(f"{ann_sha[:4].hex()} {annotations_manager.get(ann_sha).title}")
	cat_anchors(anchors, labels, filename, version_manager, version, context)
