# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

"""
List of strategies to be applied to anchors in order to updates its data.
"""

import difflib

from .anchor import *
from .diffs import *
from .globals import CONFIG
from .versionning import VersionProvider


class AnchorUpdateCandidate(NamedTuple):
	""" This tuple is a candidate for an anchor update.
	
	`anchor` can be None if the suggested update is a deletion.
	"""
	anchor: Optional[Anchor]
	confidence: float
	
	def is_deletion(self) -> bool:
		return self.anchor is None


DEFAULT_METHODS = CONFIG.get_value("diff.methods", "simple,slip,leven")
HUNK_MIN_RATIO = CONFIG.get_number("diff.ratio.hunk", 0.8, 0, 1)
LEVEN_MIN_RATIO = CONFIG.get_number("diff.ratio.levenstein", 0.8, 0, 1)


class Strategy:
	"""
	Base Strategy class. This class does nothing and is not intented to be used
	directly. All strategies should derive from this class.
	"""
	
	def __init__(self, vprovider: VersionProvider, versions: Tuple[str, str]):
		self._vprovider: VersionProvider = vprovider
		self.version_old: str = versions[0]
		self.version_new: str = versions[1]
	
	def apply(self, anchors: Dict[SHA, Anchor]) -> Dict[SHA, List[AnchorUpdateCandidate]]:
		"""
		The apply method use its logic to suggest a new position for each anchor
		of the entry.

		The new position are returned in the form of a new Anchor, <b>not
		registered in the AnchorManager</b>, and a number between 0 and 1
		to specify how sure is the position. The strategy must change the value.

		A pure anchor is returned instead of some number to respect the format
		of the original anchor, no matter what kind of information is saved
		inside. Moreover, a maximum of anchor is treated at the same time to
		increase efficiency, like global moving in the file.

		:param anchors: list of Anchors to update
		:return: a list of (the old anchor id, the new anchor and a number between 0 and 1 inclusive)
		"""
		raise NotImplementedError("Function apply not ready")


class DiffLineStrategy(Strategy):
	"""
	Update anchors where both ends are located on non modified lines.
	This strategy should place a new anchor at the same place if the file is not modified.
	
	The correlation coefficient is equal to (number of identical lines) / (old number of lines) for all the lines
	inside the anchor area.
	"""
	diff_method = True
	slip_method = True
	leven_method = True
	
	@staticmethod
	def _similar_line(old_line: str, file_content_new: List[str], start: int) -> Tuple[int, float]:
		seq_line = difflib.SequenceMatcher(lambda x: x in " \t\n")
		seq_line.set_seq2(old_line)
		best_line = start
		best_ratio = 0
		for slip in (0, 1, -1, 2, -2):
			if 0 <= start + slip < len(file_content_new):
				seq_line.set_seq1(file_content_new[start + slip])
				if seq_line.quick_ratio() >= best_ratio and seq_line.ratio() > best_ratio:
					best_line = start + slip
					best_ratio = seq_line.ratio()
		return best_line, best_ratio
	
	def apply(self, anchors: Dict[SHA, Anchor]) -> Dict[SHA, List[AnchorUpdateCandidate]]:
		# group the anchors by files
		files: Dict[str, Set[SHA]] = dict()
		for sha, anchor in anchors.items():
			if anchor.filename not in files:
				files[anchor.filename] = set()
			files[anchor.filename].add(sha)
		
		results = dict()
		main_diff = Diff(self._vprovider, self.version_old, self.version_new)
		for filename, anchor_shas in files.items():
			diff: DiffFile = main_diff.file(filename)
			
			if diff is None:
				for anchor_sha in anchor_shas:
					anchor: Anchor = anchors[anchor_sha]
					results[anchor_sha] = [
						AnchorUpdateCandidate(Anchor(self.version_new, filename, anchor.lines, anchor.line_offsets),
							1.0)]
				continue
			elif diff.patch.is_removed_file:
				for anchor_sha in anchor_shas:
					results[anchor_sha] = [AnchorUpdateCandidate(None, 1.0)]
				continue
			elif not diff.patch.is_modified_file:
				for anchor_sha in anchor_shas:
					anchor: Anchor = anchors[anchor_sha]
					results[anchor_sha] = [AnchorUpdateCandidate(
						Anchor(self.version_new, diff.target_file, anchor.lines, anchor.line_offsets), 1.0)]
				continue

			file_content_old = self._vprovider.file_content(self.version_old, filename).readlines()
			file_content_new = self._vprovider.file_content(self.version_new, filename).readlines()
			
			for anchor_sha in anchor_shas:
				anchor: Anchor = anchors[anchor_sha]
				results[anchor_sha] = list()
				
				expected_start_line = diff.translation(anchor.lines[0])
				expected_end_line = diff.translation(anchor.lines[1])
				# an anchor is moved if its two limits are on context lines
				if diff.is_context(anchor.lines[0]) and diff.is_context(anchor.lines[1]):
					lines = (expected_start_line, expected_end_line)
					coeff = diff.count_identical_lines(anchor.lines[0], anchor.lines[1]) / (lines[1] - lines[0] + 1)
					new_anchor = Anchor(self.version_new, diff.target_file, lines, anchor.line_offsets)
					new_anchor.set_state(AnchorState.CONFIRMED)
					results[anchor_sha].append(AnchorUpdateCandidate(new_anchor, coeff))
				
				elif self.diff_method:
					hunk_diffs = diff.hunk_diff(*anchor.lines)
					ratio = sum(hunk_diff.ratio(anchor.lines, anchor.line_offsets) * hunk_diff.length
						for hunk_diff in hunk_diffs) / sum(hunk_diff.length for hunk_diff in hunk_diffs)
					if ratio > HUNK_MIN_RATIO:
						start_cursor = anchor.lines[0], anchor.line_offsets[0]
						end_cursor = anchor.lines[1], anchor.line_offsets[1]
						
						for hunk_diff in hunk_diffs:
							if anchor.lines[0] + 1 < hunk_diff.end:
								start_cursor = hunk_diff.translation(anchor.lines[0], anchor.line_offsets[0], True)
							if anchor.lines[1] + 1 < hunk_diff.end:
								end_cursor = hunk_diff.translation(anchor.lines[1], anchor.line_offsets[1], False)
						
						new_anchor = Anchor(self.version_new, diff.target_file, (start_cursor[0], end_cursor[0]),
							(start_cursor[1], end_cursor[1]))
						new_anchor.set_state(AnchorState.CONFIRMED if ratio == 1.0 else AnchorState.AMBIGUOUS)
						results[anchor_sha].append(AnchorUpdateCandidate(new_anchor, ratio))
				
				if self.slip_method and len(results[anchor_sha]) == 0:
					# search for the same block code somewhere in the file
					# ONLY if the diff was not able to reposition the code
					pos = 0
					searched_content = file_content_old[anchor.lines[0]:anchor.lines[1] + 1]
					size = len(searched_content)
					while pos + size <= len(file_content_new):
						if all(file_content_new[pos + i] == searched_content[i] for i in range(size)):
							new_anchor = Anchor(self.version_new, diff.target_file, (pos, pos + size - 1),
								anchor.line_offsets)
							new_anchor.set_state(AnchorState.AMBIGUOUS)
							results[anchor_sha].append(AnchorUpdateCandidate(new_anchor, 1))
						pos += 1
				
				if self.leven_method and len(results[anchor_sha]) == 0:
					start_candidate, start_ratio = self._similar_line(file_content_old[anchor.lines[0]],
						file_content_new, expected_start_line)
					if anchor.lines[0] == anchor.lines[1]:
						end_candidate = start_candidate
						end_ratio = start_ratio
					else:
						end_candidate, end_ratio = self._similar_line(file_content_old[anchor.lines[1]],
							file_content_new, expected_end_line)
					if start_ratio > LEVEN_MIN_RATIO and end_ratio > LEVEN_MIN_RATIO:
						new_anchor = Anchor(self.version_new, diff.target_file, (start_candidate, end_candidate),
							(0, len(file_content_new[end_candidate]) - 1))
						new_anchor.set_state(AnchorState.AMBIGUOUS)
						results[anchor_sha].append(AnchorUpdateCandidate(new_anchor, start_ratio))
		
		return results
