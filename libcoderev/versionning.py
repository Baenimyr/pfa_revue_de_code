# coding:utf-8
import difflib
import subprocess
from io import TextIOWrapper, StringIO
from os import getcwd, listdir
from os.path import abspath, exists, join, relpath, split
from typing import Callable, List, Optional, TextIO, Tuple


class VersionProvider:
	"""
	This tool can give the whole content of a file in a specific version.
	It required that a versionning system was present in the working directory
	or two directory to use as the old version and the new version.
	"""
	
	def actual_version(self) -> str:
		""" Give the last known commit. """
		raise NotImplementedError()
	
	def previous_versions(self, commit: str = "HEAD") -> List[str]:
		""" If actual_version give the last commit, previous_version give the
		commit just before. So, after commiting your project, you can update
		from previous_version to actual_version.
		"""
		raise NotImplementedError()
	
	def rev_parse(self, rev: str) -> Optional[str]:
		raise NotImplementedError()
	
	def relpath(self, filename: str) -> str:
		""" Give the relative path of a file, from the root of the current
		project. If the program is not use in the root directory, paths should
		not be constant. """
		return filename
	
	def diff(self, a: str, b: str) -> TextIO:
		""" Return the diff of the complete project. """
		raise NotImplementedError()
	
	def file_content(self, version: str, filename: str) -> TextIO:
		""" Return the whole content of a file for a specific version. The
		result is a text stream (decoded using utf8), the stream must be closed
		after use.
		:return: a stream where you can read the file
		"""
		raise NotImplementedError()


class FileVersionProvider(VersionProvider):
	""" This version provider allow you to have an older version in one
	directory and the newer version in an other one. You need two path: the root
	of the older project's directory and the root of the newer. Only two
	versions are allowed "before" and "after", don't try to use something else.
	"""
	
	def __init__(self, before_dir: str, after_dir: str):
		self._before = abspath(before_dir)
		self._after = abspath(after_dir)
	
	def actual_version(self) -> str:
		return "after"
	
	def previous_versions(self, commit: str = "after") -> List[str]:
		return ["before"] if commit == "after" else []
	
	def rev_parse(self, rev: str) -> Optional[str]:
		if rev is None or rev.lower() == "head":
			return "after"
		if rev.lower() == "head~1":
			return "before"
		return rev
	
	def rename(self, version_old: str, version_new: str, filename: str):
		return join(self._after, self.relpath(filename))
	
	def relpath(self, filename: str) -> str:
		""" The filename must be relative to root of the project's directory, be
		relative to the current working dir, or be absolute. The aim is to
		relativize the path the the project's root directory. """
		f = abspath(filename)  # DO NOT DELETE this line, or the next 4 will not work anymore
		if f.startswith(self._before):
			return relpath(f, self._before)
		elif f.startswith(self._after):
			return relpath(f, self._after)
		return super(FileVersionProvider, self).relpath(filename)
	
	def diff(self, a: str, b: str) -> TextIO:
		diff = list()
		for filename in listdir(self._after):
			if exists(join(self._before, filename)):
				with self.file_content(a, filename) as content_a, self.file_content(b, filename) as content_b:
					diff.append(
						"".join(difflib.unified_diff(content_a.readlines(), content_b.readlines(), filename, filename)))
		return StringIO("\n".join(diff))
	
	def file_content(self, version: str, filename: str) -> TextIO:
		filename = self.relpath(filename)
		if version == "before":
			return open(join(self._before, filename), "r", encoding="utf8")
		elif version == "after":
			return open(join(self._after, filename), "r", encoding="utf8")
		else:
			raise KeyError("Version '{:s}' inconnue !".format(version))


class GitVersionProvider(VersionProvider):
	"""
	Interface to read version information from a git repository.
	The actual version, will be the sha of HEAD. It's more the last commited
	version.

	Git doesn't support file's renaming because it works on content not files.
	"""
	
	def __init__(self, working_dir=getcwd()):
		self._head = None
		self.repo = None
		
		while working_dir:
			if exists(join(working_dir, ".git")):
				self.repo = working_dir
			working_dir, tail = split(working_dir)
			if not tail:
				break
	
	def relpath(self, filename: str) -> str:
		f = abspath(filename)
		if self.repo and f.startswith(self.repo):
			return relpath(f, self.repo)
		return super(GitVersionProvider, self).relpath(filename)
	
	def actual_version(self) -> Optional[str]:
		if self._head is None:
			self._head = self.rev_parse("HEAD")
		return self._head
	
	def previous_versions(self, commit: str = "HEAD") -> List[str]:
		result = subprocess.run(["git", "rev-parse", commit+"^@"], stdout=subprocess.PIPE, encoding="utf-8")
		if result.returncode == 0:
			return result.stdout.splitlines(keepends=False)
		return list()
	
	def rev_parse(self, rev: str) -> Optional[str]:
		result = subprocess.run(["git", "rev-parse", rev], stdout=subprocess.PIPE, encoding="utf-8")
		if result.returncode == 0:
			return result.stdout[:-1]
		return None
	
	def diff(self, a: str, b: str) -> TextIO:
		result = subprocess.run(["git", "diff", "-M", a, b], stdout=subprocess.PIPE, encoding="utf-8")
		if result.returncode == 0:
			return StringIO(result.stdout)
	
	def file_content(self, version: str, filename: str) -> TextIO:
		result = subprocess.Popen(["git", "show", "{:s}:{:s}".format(version, filename)], stdout=subprocess.PIPE, )
		return TextIOWrapper(result.stdout, encoding="utf8")


def detect_version_system() -> Optional[VersionProvider]:
	""" Detect a versioning manager.
	Initialise the corresponding interface.
	"""
	p = getcwd()
	while p:
		if exists(join(p, ".git")):
			return GitVersionProvider()
		p, tail = split(p)
		if not tail:
			break
	return None


def iter_version_tree(v_p: VersionProvider, result: List[Tuple[str, str]], start: str, stop: Callable[[str], bool],
		_depth=20):
	"""
	Create a topological list of version transition in the tree.
	
	:param result: a list of ordered version tuples. First give an empty list. Can be reused.
	:param start: a version to start from
	:param stop: a condition to stop the depth search and start going back
	:param _depth: maximum depth. The iteration stops at the depth 0 and start collect the version
	"""
	if _depth > 0 and not stop(start):
		for parent in v_p.previous_versions(start):
			if (parent, start) not in result:
				iter_version_tree(v_p, result, parent, stop, _depth=_depth - 1)
				result.append((parent, start))


def parse_ref(version_manager: VersionProvider, code: str) -> str:
	if code is None:
		return version_manager.actual_version()
	return version_manager.rev_parse(code)

def parse_refs(version_manager: VersionProvider, versions) -> List[str]:
	if versions is None:
		return [version_manager.actual_version()]
	else:
		versions = list(map(version_manager.rev_parse, versions))
		return versions
