# coding: utf-8
# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

"""
The main goal of this module is to manipulate the anchors.
Anchors are used to locate a precise part of the code ( a given line ).
"""
import hashlib
import json
import time
from enum import Enum, unique
from os import makedirs, path, remove
from typing import *

from libcoderev.globals import root_dir

SHA = bytes


@unique
class AnchorState(Enum):
	""" Differents states of an anchor.
	CONFIRMED means the user placed it or the system is 100% sure.
	AMBIGUOUS after an automatic update but the system is not sure. The anchor
	should be at the expected position.
	ERROR after an automatic update which failed to reposition the anchor.
	The anchor should be at its old known position.
	"""
	
	CONFIRMED = "OK"
	AMBIGUOUS = "Ambiguous"
	ERROR = "Error"
	
	def __gt__(self, other):
		if self == AnchorState.CONFIRMED:
			return other == AnchorState.AMBIGUOUS or other == AnchorState.ERROR
		elif self == AnchorState.AMBIGUOUS:
			return other == AnchorState.ERROR
		else:
			return False


class Anchor:
	""" An anchor.
	An anchor can be associated with only one file, but can contains multiples
	lines of it.
	An anchor is an immutable object, it must not be modified.
	
	The anchor points to a section of a text file. This section is delimited by a character in a line (included) and an
	other character in an other line or the same (included).
	
	!!! Line numbers start at 0 but are printed from 1, like in `diff`.
	"""
	
	__slots__ = ["_creation_time", "_version", "_filename", "_lines", "_line_offsets", "_state", "data"]
	
	def __init__(self, version: str, filename: str, lines: Tuple[int, int], offsets: Tuple[int, int],
			creation_time: int = None):
		assert isinstance(lines, (tuple, list)) and len(lines) == 2 and isinstance(lines[0], int) and isinstance(
			lines[1], int)
		assert lines[0] >= 0 and lines[1] >= 0, f"The line numbers must be 2 positive digits not '{lines}'"
		assert lines[0] <= lines[1]
		assert isinstance(offsets, (tuple, list)) and len(offsets) == 2 and offsets[0] >= 0 and offsets[
			1] >= 0, f"The offset must be a tuple of 2 positive digits not '{offsets}'"
		
		self._creation_time: int = creation_time if creation_time is not None else int(time.time())
		self._version: str = version
		self._filename: str = filename
		self._lines: Tuple[int, int] = tuple(lines)
		self._line_offsets: Tuple[int, int] = tuple(offsets)
		self._state = AnchorState.CONFIRMED
		self.data: Dict[str, Any] = dict()
	
	@property
	def version(self) -> str:
		return self._version
	
	@property
	def creation_time(self) -> int:
		return self._creation_time
	
	@property
	def filename(self) -> str:
		return self._filename
	
	@property
	def lines(self) -> Tuple[int, int]:
		return self._lines
	
	@property
	def line_offsets(self) -> Tuple[int, int]:
		return self._line_offsets
	
	def set_state(self, state: AnchorState):
		""" Change the current state of the anchor.
		Allow the system or the user to confirm an ambiguous anchor.
		During an automatic update of the anchor, the state can become AMBIGUOUS
		or ERROR.
		"""
		self._state = state
	
	def get_state(self) -> AnchorState:
		""" Return the actual state of this anchor. """
		return self._state
	
	def is_at_line(self, line: int) -> bool:
		"""
		:return: true if this anchor includes the line
		"""
		return self.lines[0] <= line <= self.lines[1]
	
	def context_lines(self, margin: int = 0) -> Iterable[int]:
		"""
		Compute a set of lines helping finding the anchor in a file.
		"""
		return range(max(self.lines[0] - margin, 0), self.lines[1] + 1 + margin)
	
	def __eq__(self, other):
		return isinstance(other, Anchor) and self.version == other.version and self._filename == other._filename \
			and self._lines == other._lines and self._line_offsets == other._line_offsets
	
	def sha(self) -> SHA:
		sha = hashlib.sha256()
		sha.update(self.version.encode("utf8"))
		sha.update(self.filename.encode("utf8"))
		sha.update(self.lines[0].to_bytes(4, 'big'))
		sha.update(self.line_offsets[0].to_bytes(4, 'big'))
		sha.update(self.lines[1].to_bytes(4, 'big'))
		sha.update(self.line_offsets[1].to_bytes(4, 'big'))
		return sha.digest()
	
	def __repr__(self):
		return f"Anchor ({self.get_state()}) in {self.filename} at ({self.lines[0] + 1:d}:{self.line_offsets[0]:d}, " + \
			f"{self.lines[1] + 1:d}:{self.line_offsets[1]:d})"
	
	def __str__(self):
		return repr(self)
	
	def __hash__(self):
		return hash((self.filename, self.lines, self.line_offsets))


class AnchorManager:
	""" The AnchorManager provides an API to interact with a list of anchors.
	"""
	
	def __init__(self):
		self._anchors: Dict[SHA, Anchor] = dict()
		self._save_dir = path.join(root_dir(), ".coderev", "anchors")
		
		self._loaded_commit = set()
	
	def expands_sha(self, sha: str) -> Optional[SHA]:
		"""
		Search among all knows SHA to expand this extract.
		:return: a complete SHA-256, starting with the value, or None if not found.
		"""
		for a_sha in self.anchors:
			if a_sha.hex().startswith(sha):
				return a_sha
		return None
	
	def get_by_id(self, sha: SHA) -> Optional[Anchor]:
		"""
		Return the anchor with the correct identifier.

		:param sha: the id of the anchor.
		:return: the anchor object or None
		"""
		if sha in self._anchors:
			return self._anchors[sha]
		return None
	
	def __contains__(self, item: SHA) -> bool:
		return item in self._anchors
	
	@property
	def anchors(self) -> Iterable[SHA]:
		return iter(self._anchors.keys())
	
	def anchors_by_version(self, version: str) -> Iterable[SHA]:
		for sha, anchor in self._anchors.items():
			if anchor.version == version:
				yield sha
	
	def register_anchor(self, anchor: Anchor) -> SHA:
		"""
		Register a new anchor in the manager.
		The anchor is immutable and a SHA-256 is linked to it. This SHA is used as the anchor unique identifier.
		:return: the SHA of the anchor.
		"""
		if anchor.version not in self._loaded_commit:
			self.load_anchors(anchor.version)
		
		for sha, existing_anchor in self._anchors.items():
			if existing_anchor == anchor:
				return sha
		
		sha = anchor.sha()
		if sha not in self.anchors:
			self._anchors[sha] = anchor
		return sha
	
	def remove_anchor(self, sha: SHA):
		"""
		Destroy an anchor.
		Remove an anchor from the manager and cancel all references of it. All references of this anchor in annotations
		will be invalid.
		"""
		del self._anchors[sha]
	
	def all_in_file(self, filename: str):
		return [sha for sha, anchor in self._anchors.items() if anchor.filename == filename]
	
	# Data
	def unload(self, commit: str):
		self.save_anchors(commit)
		self._loaded_commit.remove(commit)
		anchors = list(sha for sha, anchor in self._anchors.items() if anchor.version == commit)
		for sha in anchors:
			self.remove_anchor(sha)
	
	def save_all(self):
		for commit in self._loaded_commit:
			self.save_anchors(commit)
	
	def save_anchors(self, commit: str):
		if commit not in self._loaded_commit:
			return
		data = {sha.hex(): {"ct": anchor.creation_time, "file": anchor.filename,
			"l": [anchor.lines[0], anchor.lines[1], anchor.line_offsets[0], anchor.line_offsets[1]],
			"s": anchor.get_state().value, "data": anchor.data} for sha, anchor in self._anchors.items() if
			anchor.version == commit}
		
		filepath = path.join(self._save_dir, commit + ".json")
		if not path.exists(path.dirname(filepath)):
			makedirs(path.dirname(filepath))
			
		if len(data) > 0:
			with open(filepath, "w", encoding="utf8") as save_file:
				json.dump(data, save_file, indent=None, sort_keys=False, ensure_ascii=False)
		elif path.exists(filepath):
			remove(filepath)
	
	def load_anchors(self, commit: str):
		if commit in self._loaded_commit:
			return
		self._loaded_commit.add(commit)
		
		filepath = path.join(self._save_dir, commit + ".json")
		if path.exists(filepath):
			with open(filepath, "r", encoding="utf8") as save_file:
				data = json.load(save_file)
			for sha, properties in data.items():
				anchor = Anchor(version=commit, filename=properties["file"],
					lines=(int(properties["l"][0]), int(properties["l"][1])),
					offsets=(int(properties["l"][2]), int(properties["l"][3])), creation_time=properties["ct"])
				anchor.set_state(AnchorState(properties["s"]))
				anchor.data = properties.get("data", dict())
				assert anchor.sha().hex() == sha
				self.register_anchor(anchor)
	
	def anchor_version_exists(self, commit: str) -> bool:
		""" Check if it exists some anchor data for a specific version. """
		if commit in self._loaded_commit:
			return True
		filepath = path.join(self._save_dir, commit + ".json")
		return path.exists(filepath)
