# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

from .annotation import AnnotationManager
from .interactive_resolver import InteractiveResolver
from .strategies import *
from .versionning import VersionProvider, iter_version_tree


class AnchorUpdater:
	"""
	This class is responsible of updating a maximum of anchor automatically.
	All the strategies selected by the client will be used, if needed to
	evaluate a new position for the anchor.

	The update take in entry a list of anchors to update and return a map giving
	the new anchors associated with the old anchors. The new anchors can be
	marked CONFIRMED if the algorithm is sure of the new position, AMBIGUOUS if
	not. In case the updater didn't find a new position for the anchor, a new
	anchor is returned tagged ERROR, located to the last known good position.
	"""
	
	def __init__(self, anchor_manager: AnchorManager, annotation_manager: AnnotationManager, version_provider,
			should_save=True):
		self.anchor_manager: AnchorManager = anchor_manager
		self.annotation_manager: AnnotationManager = annotation_manager
		self.version_provider: VersionProvider = version_provider
		
		self.options: Dict[str, Any] = dict()
		self.should_save: bool = should_save
	
	def update(self, versions: Tuple[str, str], auto: int = 1) -> None:
		"""
		For each anchor in the old version, find zero, one or more anchor corresponding to the new position of the code.
		An automatic processes will try to replace the anchor base on generated diff between files. It generates a list
		of possible solutions, these candidates must be filtered. The final selection can be done automatically or manually.
		
		Finally each annotation linked with an anchor A in the old version, must be linked with the selected candidates.
		:param: activate automatic update (0: pure manual, 1: partially auto, 2: full auto)
		"""
		anchors = self._filter_anchors(versions[0])
		if len(anchors) == 0:
			return
		candidates = self.apply_strategies(versions, anchors)
		
		if auto == 2:
			mono_candidates = self.resolve_automatically(candidates, versions)
		elif auto == 1:
			mono_candidates = self.resolve_partial_auto(candidates, versions)
		else:
			mono_candidates = self.resolve_interactively(candidates, versions)
		
		self.link_results(mono_candidates, versions)
		if self.should_save:
			self.anchor_manager.save_anchors(versions[1])
			self.annotation_manager.save_all()
	
	def _filter_anchors(self, version: str) -> Dict[SHA, Anchor]:
		"""
		Select anchors from the version to be update.
		
		An anchor can be updated, if it has a valid state (no ERROR) and is used by at least one opened annotation.
		:return: a dict (sha -> Anchor) of selected anchors
		"""
		anchors = dict()
		for sha in self.anchor_manager.anchors_by_version(version):
			anchor = self.anchor_manager.get_by_id(sha)
			if anchor.get_state() != AnchorState.ERROR and (self.options.get("filter.lonely", False) or any(
					self.annotation_manager.get(ann_sha).open for ann_sha in
					self.annotation_manager.get_annotations_for_anchor(sha))):
				anchors[sha] = anchor
		return anchors
	
	def apply_strategies(self, versions, anchors: Dict[SHA, Anchor]) -> Dict[SHA, List[AnchorUpdateCandidate]]:
		""" Apply strategies between the two versions. Returns candidates
		grouped by old anchor. """
		organized_candidates = {sha: list() for sha in anchors}
		
		resolver = DiffLineStrategy(self.version_provider, versions)
		resolver.diff_method = self.options.get("simple", True)
		resolver.slip_method = self.options.get("slip", True)
		resolver.leven_method = self.options.get("leven", True)
		for sha, cand in resolver.apply(anchors).items():
			organized_candidates[sha].extend(cand)
		
		return organized_candidates
	
	def resolve_interactively(self, candidates, versions) -> Dict[SHA, List[Anchor]]:
		""" Use the interactive resolution method. """
		resolver = InteractiveResolver(self.anchor_manager, self.version_provider, versions)
		return resolver.loop(candidates)
	
	def resolve_partial_auto(self, candidates: Dict[SHA, List[AnchorUpdateCandidate]], versions: Tuple[str, str]) -> \
			Dict[SHA, List[Anchor]]:
		result = {}
		error = list()
		for anchor, anchor_candidates in candidates.items():
			if len(anchor_candidates) != 1 or anchor_candidates[0][1] < CONFIG.get_number("update.auto", 0.8, 0, 1):
				error.append(anchor)
			elif not anchor_candidates[0].is_deletion():
				result[anchor] = [anchor_candidates[0][0]]
		
		manual = InteractiveResolver(self.anchor_manager, self.version_provider, versions)
		result.update(manual.loop({sha: candidates[sha] for sha in error}))
		return result
	
	def resolve_automatically(self, candidates: Dict[SHA, List[AnchorUpdateCandidate]], versions: Tuple[str, str]) -> \
			Dict[SHA, List[Anchor]]:
		""" Use the automatic resolution method.
		Only the candidates with the same maximum coefficient are kept.
		"""
		result = {}
		for anchor, new_anchors_candidates in candidates.items():
			best_coef = max(coef for candidat, coef in new_anchors_candidates) if len(
				new_anchors_candidates) > 0 else -1
			if best_coef >= CONFIG.get_number("update.auto", 0.8, 0, 1):
				result[anchor] = list(
					candidat for candidat, coef in new_anchors_candidates if coef == best_coef and candidat is not None)
			else:
				old_anchor = self.anchor_manager.get_by_id(anchor)
				new_anchor = Anchor(versions[1], old_anchor.filename, old_anchor.lines, old_anchor.line_offsets)
				new_anchor.set_state(AnchorState.ERROR)
				result[anchor] = [new_anchor]
		return result
	
	def link_results(self, mono_candidates: Dict[SHA, List[Anchor]], versions: Tuple[str, str]):
		""" Registers all new anchors in the AnchorManager and links the annotations with the anchors. """
		for sha, anchors in mono_candidates.items():
			new_shas = list()
			for anchor in anchors:
				if anchor is not None:
					assert anchor.version == versions[1]
					new_shas.append(self.anchor_manager.register_anchor(anchor))
			for ann_sha in self.annotation_manager.get_annotations_for_anchor(sha):
				for new_sha in new_shas:
					self.annotation_manager.get(ann_sha).bind_anchor(new_sha)


def update(version_manager: VersionProvider, anchor_manager: AnchorManager, annotation_manager: AnnotationManager,
		versions: Tuple[Optional[str], str], strategies=None, update_mode: int = 1, jump: bool = False,
		dry_run: bool = False):
	"""
	Find an update path between 2 commits.
	Anchors must be updated from a commit to another, one at the time.
	:param version_manager: provide commits informations
	:param anchor_manager: anchors are loaded from the last known commit.
	:param versions: (version_from, version_to)
	:param strategies: list of strategies name to use
	:param update_mode: how to manage updated anchors
	:param dry_run: new anchors are not saved
	:param jump: ignore intermediate versions
	"""
	if jump:
		path = [(versions[0], versions[1])]
		if not anchor_manager.anchor_version_exists(versions[0]):
			exit(0)
	else:
		path = list()
		if versions[0]:
			condition = lambda v: v == versions[0]
		else:
			condition = anchor_manager.anchor_version_exists
		iter_version_tree(version_manager, path, versions[1], condition)
		if len(path) == 0:
			print("No update needed.")
			exit(0)
		if all(not anchor_manager.anchor_version_exists(v[0]) for v in path):
			print(f"Unable to find a valid ancestor to update from for '{versions[1]}'.")
			exit(1)
	
	print(f"Update from {path[0][0]} to {path[-1][1]}.")
	updater = AnchorUpdater(anchor_manager, annotation_manager, version_manager, should_save=not dry_run)
	if strategies is not None:
		updater.options.update({strat: True for strat in strategies})
	
	for previous_commit, next_commit in path:
		if anchor_manager.anchor_version_exists(previous_commit):
			anchor_manager.load_anchors(previous_commit)
			anchor_manager.load_anchors(next_commit)
			print(f"########## Update {previous_commit} -> {next_commit}:")
			updater.update((previous_commit, next_commit), auto=update_mode)
			if not dry_run:
				anchor_manager.save_anchors(next_commit)
