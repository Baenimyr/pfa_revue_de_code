# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.
"""
This module provide the possibility to compute diffs between two files.
The diffs are created using the `unidiff` python module, and are bufferised to
reduce computation.
"""
from difflib import SequenceMatcher
from typing import *

import unidiff

from .versionning import VersionProvider


class Diff:
	""" Base class for computing diffs. """
	
	def __init__(self, vprovider: VersionProvider, old_hash: str, new_hash: str):
		self.vprovider = vprovider
		self.old_hash = old_hash
		self.new_hash = new_hash
		self.patchset = unidiff.PatchSet(vprovider.diff(old_hash, new_hash))
	
	def file(self, filename: str) -> Optional["DiffFile"]:
		for patch in self.patchset:
			if patch.source_file[2:] == filename:
				return DiffFile(patch)
		return None


class DiffFile:
	""" Read the old and the new version of a file and compute the standard diff.
	The result (hunks) is stored on the patch_set variable.
	"""
	
	def __init__(self, patch: unidiff.PatchedFile):
		self.patch = patch
	
	@property
	def target_file(self) -> str:
		return self.patch.target_file[2:]
	
	def is_context(self, line_no: int) -> bool:
		"""
		Check if a line was modified or just context.
		"""
		hunk: unidiff.Hunk
		for hunk in self.patch:
			if hunk.source_start <= line_no + 1 < hunk.source_start + hunk.source_length:
				return any(line.is_context and line.source_line_no == line_no + 1 for line in hunk)
		return True
	
	def translation(self, line_no: int) -> Optional[int]:
		"""
		Compute the new position of a line.
		A non-modified line can be repositioned precisely. Return the expected place for a deleted line.
		:return: the new line number
		"""
		translation = 0  # line number translation due to hunk expanding or shrinking
		near_hunk = 0  # first hunk before the line considered
		for hunk in self.patch:
			if hunk.source_start <= line_no + 1 < hunk.source_start + hunk.source_length:
				near_remaining_line = None
				for line in hunk:
					if line.is_context:
						if line.source_line_no == line_no + 1:  # The line is inside a hunk
							return line.target_line_no - 1
						elif line.source_line_no < line_no + 1 and (
								near_remaining_line is None or near_remaining_line.source_line_no < line.source_line_no):
							near_remaining_line = line
				return line_no if near_remaining_line is None else line_no - near_remaining_line.source_line_no + near_remaining_line.target_line_no
			
			if hunk.source_start + hunk.source_length <= line_no + 1 and hunk.source_start > near_hunk:
				near_hunk = hunk.source_start
				translation = (hunk.target_start + hunk.target_length) - (hunk.source_start + hunk.source_length)
		
		return line_no + translation
	
	def count_identical_lines(self, start: int, end: int) -> int:
		"""
		Count the number of non-deleted and non-added lines between the points `start` and `end` inclusive.
		"""
		total = end - start + 1
		for hunk in self.patch:
			if start < hunk.source_start - 1 + hunk.source_length or hunk.source_start - 1 <= end:
				for line in hunk:
					if line.is_removed and start <= line.source_line_no - 1 <= end:
						total -= 1
		return total
	
	def hunk_diff(self, source_start: int, source_end: int) -> List["HunkDiff"]:
		hunks = list()
		for hunk in self.patch:
			if source_start + 1 >= hunk.source_start + hunk.source_length or source_end + 1 < hunk.source_start:
				continue
			hunks.append(HunkDiff(hunk))
		return hunks
	
	def __repr__(self):
		return f"DiffFile '{self.patch.source_file}' '{self.patch.target_file}'"


class HunkDiff:
	
	def __init__(self, hunk: unidiff.Hunk):
		self.hunk = hunk
		self.content_old = "".join(map(lambda line: line.value,
			sorted(filter(lambda line: line.is_context or line.is_removed, hunk), key=lambda l: l.source_line_no)))
		self.content_new = "".join(map(lambda line: line.value,
			sorted(filter(lambda line: line.is_context or line.is_added, hunk), key=lambda l: l.target_line_no)))
		self.sequence = SequenceMatcher(autojunk=False)
		self.sequence.set_seq1(self.content_old)
		self.sequence.set_seq2(self.content_new)
	
	def convert_line(self, line: int, offset: int) -> int:
		if line + 1 < self.hunk.source_start:
			return 0
		
		for line_diff in self.hunk:
			if (line_diff.is_context or line_diff.is_removed) and line_diff.source_line_no - 1 < line:
				offset += len(line_diff.value)
		return offset
	
	def convert_pos(self, pos: int) -> Tuple[int, int]:
		line = self.hunk.source_start - 1
		for line_diff in self.hunk:
			if line_diff.is_context or line_diff.is_added:
				if pos - len(line_diff.value) < 0:
					return line_diff.target_line_no - 1, pos
				pos -= len(line_diff.value)
				line += 1
				if pos == 0:
					return line_diff.target_line_no, pos
		return line, pos
	
	def _translation(self, pos: int, right=False) -> int:
		last_pos = pos
		# matches are in ascendant order
		for match in self.sequence.get_matching_blocks():
			if right:
				if match.a <= pos < match.a + match.size:
					return pos - match.a + match.b
				elif match.a > pos:
					return match.b
			else:
				if match.a < pos <= match.a + match.size:
					return pos - match.a + match.b
				elif match.b + match.size < pos:
					last_pos = match.b + match.size
		return last_pos
	
	def translation(self, line: int, line_offset: int, right=False) -> Tuple[int, int]:
		"""
		Find the new position of a cursor.
		It can be in a modified zone or exactly where some text has been added.
		:param line: line of the cursor
		:param line_offset: offset of the cursor in the line
		:param right: align at the right edge of the unknown area, else align left.
		"""
		if line + 1 < self.hunk.source_start:
			return line + self.hunk.target_start - self.hunk.source_start, line_offset
		if line + 1 >= self.hunk.source_start + self.hunk.source_length:
			return line + self.hunk.target_start - self.hunk.source_start + self.hunk.target_length - self.hunk.source_length, line_offset
		
		pos = self.convert_line(line, line_offset)
		t_pos = self._translation(pos, right=right)
		return self.convert_pos(t_pos)
	
	def ratio(self, lines: Tuple[int, int], offsets: Tuple[int, int]) -> float:
		if lines[0] == lines[1] and offsets[0] == offsets[1]:
			return 1.0
		
		p = self.convert_line(lines[0], offsets[0])
		q = self.convert_line(lines[1], offsets[1])
		return sum(min(q, m.a + m.size) - max(p, m.a) for m in self.sequence.get_matching_blocks() if
			m.a <= q and p <= m.a + m.size) / (q - p)
	
	@property
	def length(self) -> int:
		return self.hunk.source_length
	
	@property
	def end(self) -> int:
		return self.hunk.source_start + self.hunk.source_length
	
	def __repr__(self):
		text = f"@@ -{self.hunk.source_start},{self.hunk.source_length} +{self.hunk.target_start},{self.hunk.target_length} @@\n"
		a, b = 0, 0
		for match in self.sequence.get_matching_blocks():
			if b < match.b:
				if a == match.a:
					text += "\033[30;42m"
				else:
					text += "\033[30;47m"
				text += self.content_new[b:match.b]
				b = match.b
			text += "\033[0m" + self.content_new[b:b + match.size]
			a = match.a + match.size
			b = match.b + match.size
		return text
