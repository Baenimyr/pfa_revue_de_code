# coding:utf8
from os import path, getcwd

from .config import Configuration


def find_coderev_dir():
	p = path.abspath(getcwd())
	while path.dirname(p) != p:
		if path.exists(path.join(p, ".coderev")):
			return p
		p = path.dirname(p)

def root_dir() -> str:
	""" Detect the root dir of the project and the data directory """
	p = find_coderev_dir()
	if p is None:
		print("This directory is not located in a project.")
		exit(2)
	return p
	

root = find_coderev_dir()
if root:
	CONFIG = Configuration(path.join(root, ".coderev", "config.toml"))
else:
	CONFIG = Configuration(None)
CONFIG.load()
# CONFIG.save()
