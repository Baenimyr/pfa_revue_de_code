# coding:utf8
from os.path import exists
from typing import *

import toml

CONFIG = TypeVar('CONFIG', str, int, float, list)


class RecursiveDict(dict):
	""" A recursive dict split key on the `.` letter and use the left key on the sub recursive dict. """
	def __init__(self, seq=None):
		super(RecursiveDict, self).__init__()
		if seq:
			for key in seq:
				self[key] = seq[key]
	
	def __getitem__(self, item: str) -> CONFIG:
		i = item.find(".")
		if i != -1:
			return self[item[:i]][item[i + 1:]]
		else:
			return super(RecursiveDict, self).__getitem__(item)
	
	def __setitem__(self, key: str, value: Union[CONFIG, dict]):
		i = key.find(".")
		if i != -1:
			if key[:i] not in self:
				self[key[:i]] = RecursiveDict()
			self[key[:i]][key[i + 1:]] = value
		else:
			if isinstance(value, dict):
				super(RecursiveDict, self).__setitem__(key, RecursiveDict(value))
			elif key in self and isinstance(self[key], RecursiveDict):
				raise KeyError(f"{key[:i]} is a table and cannot be replaced by a non-table.")
			else:
				super(RecursiveDict, self).__setitem__(key, value)
	
	def __delitem__(self, key: str):
		i = key.find(".")
		if i != -1:
			del self[key[:i]][key[i + 1:]]
			if len(self[key[:i]]) == 0:
				del self[key[:i]]
		else:
			super(RecursiveDict, self).__delitem__(key)
	
	def __contains__(self, item: str) -> bool:
		i = item.find(".")
		if i != -1:
			return super(RecursiveDict, self).__contains__(item[:i]) and item[i + 1:] in self[item[:i]]
		else:
			return super(RecursiveDict, self).__contains__(item)
	
	def keys(self) -> Iterable[str]:
		for key, value in super(RecursiveDict, self).items():
			if isinstance(value, RecursiveDict):
				for sub_key in value.keys():
					yield key + "." + sub_key
			else:
				yield key
				
	def __len__(self):
		total = 0
		for key, value in super(RecursiveDict, self).items():
			if isinstance(value, RecursiveDict):
				total += len(value)
			else:
				total += 1
		return total


class Configuration:
	""" This object represent a configuration file.
	This configuration system store values under the form ("key.subkey" -> value). Only the non-default parameters are
	written into the file. To reset a value to its default value, use `reset_value`. Each modification of the config
	triggers the backup.
	
	Keys are organised and displayed in the file by group: the separator in the complete key is the `.` but if a key
	`master.group.key` exists, it is impossible to store a value under `master.group`.
	"""
	
	def __init__(self, file: Optional[str]):
		self._values = RecursiveDict()
		self.file = file
	
	def keys(self):
		"""
		:return: the list of all keys registered.
		"""
		return self._values.keys()
	
	def set_value(self, key: str, value: CONFIG):
		self._values[key] = value
		self.save()
	
	def reset_value(self, key: str):
		"""
		Unset the key. The key will no be present in the config file and `get_value` will return the default value.
		"""
		if key in self._values:
			del self._values[key]
			self.save()
	
	def get_value(self, key: str, default: CONFIG = None) -> CONFIG:
		""" Return the value linked to `key` or `default if not set. """
		if key in self._values:
			return self._values[key]
		else:
			return default
	
	def get_number(self, key: str, default: float, minimum: float, maximum: float) -> float:
		""" Return the value linked to `key`.
		If it is not set or is not a number, return `default`.
		Else the value must be included between `minimum` and `maximum`, else the closed valid number will be used instead.
		
		:param default: value for no set key or not a number.
		:param minimum: None for no limit, else the returned number will be >= minimum
		:param maximum: None for no limit, else the returned number will be <= maximum
		"""
		if key not in self._values:
			return default
		value = self._values[key]
		if not isinstance(value, (int, float)):
			try:
				value = float(value)
			except ValueError:
				return default
		elif minimum and value < minimum:
			self.set_value(key, minimum)
			return minimum
		elif maximum and value > maximum:
			self.set_value(key, maximum)
			return maximum
		else:
			return value
	
	def save(self):
		""" Save the configurations into the file. """
		if self.file:
			with open(self.file, "w") as file:
				toml.dump(self._values, file, encoder=toml.TomlEncoder(dict))
	
	def load(self):
		""" Load the configuration values from the opened file. """
		if self.file and exists(self.file):
			with open(self.file, "r") as file:
				self._values = RecursiveDict(toml.load(file))
