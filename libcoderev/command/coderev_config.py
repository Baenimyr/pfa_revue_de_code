# coding: utf-8

import argparse
import re

from libcoderev.globals import CONFIG

INTEGER = re.compile("[0-9]+")
DECIMAL = re.compile("[0-9]*\\.[0-9]+")

def create_subparse_config(parser: argparse.ArgumentParser):
	parser.add_argument("key", nargs="?")
	parser.add_argument("value", nargs="?")
	
	parser.add_argument("--list", action='store_true')
	parser.add_argument("--unset", action='store_true')
	
def run(arguments: argparse.Namespace):
	if arguments.unset:
		CONFIG.reset_value(arguments.key)
	elif arguments.list:
		for key in CONFIG.keys():
			print(f"{key}={CONFIG.get_value(key)}")
	
	elif arguments.value:
		if INTEGER.fullmatch(arguments.value):
			CONFIG.set_value(arguments.key, int(arguments.value))
		elif DECIMAL.fullmatch(arguments.value):
			CONFIG.set_value(arguments.key, float(arguments.value))
		elif arguments.value.lower() == "true":
			CONFIG.set_value(arguments.key, True)
		elif arguments.value.lower() == "false":
			CONFIG.set_value(arguments.key, False)
		else:
			CONFIG.set_value(arguments.key, arguments.value)
	elif arguments.key in CONFIG.keys():
		print(CONFIG.get_value(arguments.key))
