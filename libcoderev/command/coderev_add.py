# coding:utf8
import argparse
import os.path
from sys import stderr
from typing import List, Tuple

from libcoderev import Anchor, AnchorManager, Annotation, AnnotationManager, SHA
from libcoderev.globals import CONFIG
from libcoderev.versionning import VersionProvider, parse_ref


def create_subparser_add(subparsers):
	parser = subparsers.add_parser("add", help="Create an annotation or an anchor.",
		description="""Use this command to create new anchors and new annotations.
		If an anchor is created at the same time than an annotation, the anchor is bound to the annotation.""")
	parser.add_argument("--print", "-p", action="store_true",
		help="Display the anchors and annotations ids after creation, in the same order (anchors then annotations).")
	g_ref = parser.add_argument_group("anchor")
	g_ref.add_argument("--anchor", nargs="+", metavar=("FILE", "LINE[:OFFSET]"),
		help="Create a new anchor. If present, can be use to create a new annotation.")
	g_ref.add_argument("--version", "-V", type=str, help="Select a different version from the current version")
	
	g_ann = parser.add_argument_group("annotation")
	g_ann.add_argument("--annotation", action="store_true", help="Create a new annotation")
	g_ann.add_argument("--title", "-t", nargs="?", dest="title", type=str, help="Title for the annotation")
	g_ann.add_argument("--tags", nargs="*", dest="tags", type=str, help="List of tags for the annotation")
	g_ann.add_argument("--description", "-d", type=str, help="Description for the annotation")
	g_ann.add_argument("--ref", nargs="*", metavar="hash", type=str,
		help="identifiers of anchors to use. An anchor is required, use --ref or --anchor to provide an anchor")


def check_offsets(lines: List[str], start: Tuple[int, int], stop: Tuple[int, int]) -> Tuple[Tuple[int, int], Tuple[int, int]]:
	"""
	Checks that
	- the line exists
	- the offset is not out of bound 0 <= offset <= len(lines[line])
	
	If the offset is not given (== -1), it can be estimated.
	For the offset of the anchor start, it will be placed after the heading whitespaces/tabs.
	For the offset of the anchor end, it will be placed before the trailing whitespaces/tabs.
	
	:param lines: all the lines of the file
	:param start: first cursor of the anchor
	:param stop: last cursor of the anchor
	:return: the corrected cursors
	"""
	transform_tabs = CONFIG.get_value("gui.tabs", True)
	tabs_length = CONFIG.get_value("gui.tablength", 4) - 1
	
	line_0, offset_0 = start
	line_1, offset_1 = stop
	
	if line_0 < 0 or line_0 >= len(lines):
		raise IndexError(f"The line {line_0 + 1} doesn't exist !")
	if line_1 < 0 or line_1 >= len(lines):
		raise IndexError(f"The line {line_1 + 1} doesn't exists !")
	
	if offset_0 < 0:
		offset_0 = 0
		while offset_0 + 1 < len(lines[line_0]) and lines[line_0][offset_0] in " \t":
			offset_0 += 1
	elif transform_tabs:
		offset_0 -= 1
		i = 0
		while i < len(lines[line_0]) and i < offset_0 and lines[line_0][i] == "\t":
			i += 1
			offset_0 -= tabs_length
	if offset_0 >= len(lines[line_0]):
		raise IndexError(f"Offset out of bound: {offset_0} at line {line_0 + 1}")
	
	if offset_1 < 0:
		offset_1 = len(lines[line_1]) - 1
		while offset_1 > offset_0 and lines[line_1][offset_1 - 1] in " \t":
			offset_1 -= 1
	elif transform_tabs:
		offset_1 -= 1
		i = 0
		while i < len(lines[line_1]) and i < offset_1 and lines[line_1] == "\t":
			i += 1
			offset_1 -= tabs_length
			
	if offset_1 >= len(lines[line_1]):
		raise IndexError(f"Offset out of bound: {offset_1} at line {line_1 + 1}")
	
	if line_0 > line_1 or (line_0 == line_1 and offset_0 > offset_1):
		raise ValueError(f"The interval must not be reversed: ({line_0 + 1}:{offset_0}) => ({line_1 + 1}:{offset_1})")
	
	return (line_0, offset_0), (line_1, offset_1)


def execution(arguments: argparse.Namespace, version_provider: VersionProvider):
	# Assert file exists
	if arguments.anchor and not os.path.exists(arguments.anchor[0]):
		print("Unknow file: '" + arguments.anchor[0] + "'", file=stderr)
		exit(1)
	
	# Loading anchors and annotations
	version_id = parse_ref(version_provider, arguments.version)
	anchor_manager: AnchorManager = AnchorManager()
	anchor_manager.load_anchors(version_id)
	
	anchors: List[SHA] = []
	
	# Anchors from the --ref arg
	if arguments.ref:
		for anchor_id in arguments.ref:
			# check existence
			sha = anchor_manager.expands_sha(anchor_id)
			if sha is not None:
				anchors.append(sha)
			else:
				print("Error: anchor {} not found".format(anchor_id), file=stderr)
				exit(2)
	
	# Anchors from the --anchor arg
	if arguments.anchor:
		# Creating anchor
		anchor = arguments.anchor
		filename = anchor[0]
		line_0, line_1 = -1, -1
		offset_0, offset_1 = -1, -1
		
		if len(anchor) >= 2:
			pos1 = str(anchor[1])
			if ":" in pos1:
				s = pos1.index(":")
				line_0 = line_1 = int(pos1[:s]) - 1
				offset_0 = int(pos1[s + 1:])
			else:
				line_0 = line_1 = int(pos1) - 1
		else:
			print("I need at least one line to place the anchor.", file=stderr)
			exit(3)
		
		if len(anchor) >= 3:
			pos2 = anchor[2]
			if ":" in pos2:
				s = pos2.index(":")
				line_1 = int(pos2[:s]) - 1
				offset_1 = int(pos2[s + 1:])
			else:
				line_1 = int(pos2) - 1
				
		with version_provider.file_content(version_id, filename) as file_content:
			lines = file_content.readlines()
			try:
				start, end = check_offsets(lines, (line_0, offset_0), (line_1, offset_1))
				line_0, offset_0 = start
				line_1, offset_1 = end
			except Exception as ie:
				print(ie, file=stderr)
				exit(2)
				
		if line_0 == line_1 and offset_0 == offset_1:
			print(f"No text selected between {line_0 + 1}:{offset_0} and {line_1 + 1}:{offset_1} !", file=stderr)
			exit(2)
			
		anchor = Anchor(version_id, filename, (line_0, line_1), (offset_0, offset_1))
		if "user.name" in CONFIG.keys():
			anchor.data["author"] = CONFIG.get_value("user.name") + " <" + CONFIG.get_value("user.email", "?") + ">"
		anchor_id = anchor_manager.register_anchor(anchor)
		anchors.append(anchor_id)
		
		if arguments.print:
			print(anchor_id.hex())
	
	if arguments.annotation:
		annotation_manager = AnnotationManager()
		annotation = Annotation(title=arguments.title, description=arguments.description, tags=arguments.tags)
		annotation_sha = annotation_manager.register_annotation(annotation)
		for a in anchors:
			annotation.bind_anchor(a)
		if arguments.print:
			print(annotation_sha.hex())
		
		annotation_manager.save_all()
	
	# Saving to disk
	anchor_manager.save_all()
