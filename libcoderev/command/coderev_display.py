# coding:utf8
import argparse
import sys

from libcoderev import *
from libcoderev import Annotation, AnnotationManager
from libcoderev.versionning import VersionProvider, parse_ref


def create_subparser_display(subparsers):
	parser = subparsers.add_parser("display", help="Display information about annotations and anchors.", description="""Display anchors or annotations data, like unique identifiers.
		Anchors show their id, files and lines.
		Annotations show their id, title and tags in simple mode, the description and anchors list in verbose mode.
		
		Filter: results can be limited to a known id, a file, or a file with a line: only anchors which include these
		lines will be displayed.""")
	sub = parser.add_subparsers(dest="display_action")
	
	annotation = sub.add_parser("annotation", help="Display for annotations")
	annotation.add_argument("--all", "-a", action="store_true", default=False,
		help="Display all available (including inactive annotations), restricted by file filter.")
	annotation.add_argument("--file", "-f", nargs="+", metavar="file",
		help="restrict to annotations present in one of the files.")
	# annotation.add_argument("--line", "-l", nargs="+", type=int, metavar="line",
	# 	help="restrict to annotations present in a file at a specified line.")
	annotation.add_argument("--version", "-V", nargs="*", type=str, help="Select a different version")
	annotation.add_argument("--verbose", "-v", action="count", default=0, help="verbose mode")
	annotation.add_argument("id", nargs="*", help="Select an annotation by its SHA.")
	
	reference = sub.add_parser("anchor", help="Display for anchors",
		description="""All corresponding anchors are printed in a table in the order:\n
			'sha' 'start line' 'end line' 'file' 'state' 'source version'\n
			The number of columns may vary with verbose.""")
	reference.add_argument("--all", "-a", action="store_true", help="Display all available in the current version.")
	reference.add_argument("--file", "-f", nargs="+", metavar="file", help="restrict to anchors present in a files.")
	reference.add_argument("--line", "-l", nargs="+", type=int, metavar="line",
		help="restrict to annotations present in a file at a specified line.")
	reference.add_argument("--error", action="store_true", help="Show anchors in error state.")
	reference.add_argument("--version", "-V", nargs="*", type=str, help="Select a different version")
	reference.add_argument("--verbose", "-v", action="count", default=0, help="verbose mode")
	reference.add_argument("id", nargs="*", help="Select an anchor by its SHA.")


def execution(args: argparse.Namespace, version_manager: VersionProvider):
	if args.version is not None:
		try:
			versions = [parse_ref(version_manager, v) for v in args.version]
			if any(v is None for v in versions):
				exit(2)
		except ValueError as v:
			print(v, file=sys.stderr)
			exit(2)
			return
	else:
		versions = [version_manager.actual_version()]
	
	anchor_manager = AnchorManager()
	for version in versions:
		anchor_manager.load_anchors(version)
	
	if args.display_action == "annotation":
		annotation_manager = AnnotationManager()
		list_id = list()
		
		# filter by id
		if args.id is not None and len(args.id) > 0:
			for id in args.id:
				sha = annotation_manager.expands_sha(id)
				if sha is not None:
					list_id.append(sha)
				else:
					print("'{}' is not a valid id.".format(id), file=sys.stderr)
					exit(1)
		
		# filter by state, or file
		else:
			for sha in annotation_manager.get_ids():
				annotation = annotation_manager.get(sha)
				if (args.all is True or annotation.open) and (
						args.file is None or any(f in args.file for f in annotation.files(anchor_manager))):
					list_id.append(sha)
		
		for sha in sorted(list_id):
			ann: Annotation = annotation_manager.get(sha)
			if args.verbose == 0:
				print(f"{sha[:8].hex()} '{ann.title}'")
			elif args.verbose == 1:
				print(sha.hex())
				print("\t'" + ann.title + "'")
				print(f"tags: {','.join(ann.tags)}")
				print(f"description: {ann.description if ann.description else ''}")
				print()
			else:
				print(sha.hex())
				print("\t'" + ann.title + "'")
				print(f"tags: {','.join(ann.tags)}")
				print(f"description: {ann.description if ann.description else ''}")
				print(f"anchors:")
				anchors = sorted(
					filter(lambda a: a[1] is not None and a[1].version in versions,
					map(lambda sha: (sha, anchor_manager.get_by_id(sha)), ann.anchors)),
					key=lambda a: versions.index(a[1].version))
				for anchor_sha, anchor in anchors:
					print(f"{anchor_sha[:8].hex()}\t{anchor.lines[0]}:{anchor.line_offsets[0]}\t{anchor.lines[1]}:"
						f"{anchor.line_offsets[1]}\t{anchor.filename}")
				print()
	
	if args.display_action == "anchor":
		if args.all:
			display_list = anchor_manager.anchors
		elif args.id is not None and len(args.id) > 0:
			display_list = list()
			for id in args.id:
				sha = anchor_manager.expands_sha(id)
				if sha is not None:
					display_list.append(sha)
		elif args.file and len(args.file) > 0:
			display_list = list()
			for sha in anchor_manager.anchors:
				anchor = anchor_manager.get_by_id(sha)
				if anchor.filename in args.file and (
						args.line is None or any(anchor.lines[0] <= line <= anchor.lines[1] for line in args.line)):
					display_list.append(sha)
		else:
			return
		
		if args.error:
			display_list = filter(lambda sha: anchor_manager.get_by_id(sha).get_state() == AnchorState.ERROR,
				display_list)
		
		if args.verbose == 0:
			for anchor_sha in display_list:
				print(f"{anchor_sha[:12].hex()}")
		else:
			for anchor_sha in display_list:
				anchor = anchor_manager.get_by_id(anchor_sha)
				print(f"{anchor_sha[:16].hex()} {anchor.lines[0] + 1:>4}:{anchor.line_offsets[0]:<3} "
					f"{anchor.lines[1] + 1:>4}:{anchor.line_offsets[1]:<3} '{anchor.filename}' "
					f"{anchor.get_state().value:<10} {anchor.version[:6]}")
