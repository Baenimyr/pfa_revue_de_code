# coding:utf8

import argparse
import sys

from libcoderev.annotation import *
from libcoderev.versionning import VersionProvider


def create_subparser_edit(subparsers):
	parser: argparse.ArgumentParser = subparsers.add_parser("edit", help="Help you edit annotations and anchors.")
	
	sub = parser.add_subparsers(dest="object")
	annotation = sub.add_parser("annotation")
	annotation.add_argument("sha", type=str)
	annotation.add_argument("--tags", nargs="+", help="""Change the tags.
	Add a tag by giving its name. To remove a tag, prefix it with a '^' (ex: ^TODO).
	All tags are case-sensitive.""")
	annotation.add_argument("--description", type=str, help="""Modify the description.
	'.' or 'null' means an empty description.""")
	annotation.add_argument("--close", action="store_true",
		help="""Close an annotation. A closed annotation will not be displayed by default and cannot be modified.""")
	annotation.add_argument("--delete", "-d", action="store_true", help="Delete the annotation.")
	
	anchors = sub.add_parser("anchor")
	anchors.add_argument("sha", type=str)
	group = anchors.add_mutually_exclusive_group()
	group.add_argument("--state", type=str, choices=["error", "ambiguous", "confirmed", "e", "a", "c"],
		help="Change the anchor state")
	group.add_argument("--delete", "-d", action="store_true", help="Delete the anchor.")
	group.add_argument("--replace", metavar="NEW", help="Replace the anchor and all its associations by a new one."
		"The old anchor is not destroyed but not used anymore.")


def _edit_anchor(args: argparse.Namespace, version_manager: VersionProvider):
	anchor_manager = AnchorManager()
	annotation_manager = AnnotationManager()
	
	anchor_manager.load_anchors(version_manager.actual_version())
	anchor_sha = anchor_manager.expands_sha(args.sha)
	if anchor_sha is None:
		print(f"Unknown anchor id: {args.sha}", file=sys.stderr)
		exit(1)
		
	if args.delete:
		for annotation_sha in annotation_manager.get_annotations_for_anchor(anchor_sha):
			annotation = annotation_manager.get(annotation_sha)
			annotation.unbind_anchor(anchor_sha)
			annotation_manager.save_annotation(annotation_sha)
		anchor_manager.remove_anchor(anchor_sha)
		
	elif args.replace:
		new_sha = anchor_manager.expands_sha(args.replace)
		if new_sha is None:
			print(f"Unknown anchor id: {args.sha}", file=sys.stderr)
			exit(1)
			
		for ann in annotation_manager.get_annotations_for_anchor(anchor_sha):
			annotation = annotation_manager.get(ann)
			annotation.bind_anchor(new_sha)
			annotation.unbind_anchor(anchor_sha)
			annotation_manager.save_annotation(ann)
		
	elif args.state:
		anchor = anchor_manager.get_by_id(anchor_sha)
		anchor.set_state(AnchorState.CONFIRMED if args.state.startswith("c") else AnchorState.AMBIGUOUS
			if args.state.startswith("a") else AnchorState.ERROR)
		
	anchor_manager.save_anchors(version_manager.actual_version())


def _edit_annotations(args: argparse.Namespace, version_manager: VersionProvider):
	annotation_manager: AnnotationManager = AnnotationManager()
	sha = annotation_manager.expands_sha(args.sha)
	if sha is None:
		print(f"Unknown annotation id: {args.sha}", file=sys.stderr)
		exit(1)
	
	annotation = annotation_manager.get(sha)
	if args.delete:
		annotation_manager.delete_annotation(sha)
		exit(0)
	
	if not annotation.open:
		print(f"The annotation '{sha[:16].hex()}' is closed.")
		exit(2)
	
	if args.tags:
		for tag in args.tags:
			if tag.startswith("^"):
				annotation.tags.remove(tag[1:])
			else:
				annotation.tags.add(tag)
	
	if args.description:
		if args.description == "." or args.description.lower() == "null":
			annotation.description = None
		else:
			annotation.description = args.description
	
	annotation_manager.save_annotation(sha)


def execution(args: argparse.Namespace, version_manager: VersionProvider):
	if args.object == "anchor":
		_edit_anchor(args, version_manager)
	if args.object == "annotation":
		_edit_annotations(args, version_manager)
