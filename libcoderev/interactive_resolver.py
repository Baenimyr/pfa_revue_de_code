# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.

import logging
from os.path import isfile
from typing import *

import libcoderev as a
from . import display
from .anchor import Anchor, AnchorManager, SHA
from .strategies import AnchorUpdateCandidate
from .versionning import VersionProvider

logging.basicConfig(level=logging.INFO)

BOLD = "\033[1m"
RESET = "\033[0m"
FAINT = "\033[2m"


class InteractiveResolver:
	# FIXME: need fixing of `updater` and strategies to test this UI
	def __init__(self, anchor_manager: AnchorManager, version_provider: VersionProvider, versions: Tuple[str, str]):
		self.anchor_manager: AnchorManager = anchor_manager
		self.current_anchor_sha: Optional[SHA] = None
		self.current_anchor: Optional[Anchor] = None
		self.current_candidates: List[AnchorUpdateCandidate] = list()
		self.results: Dict[SHA, List[Anchor]] = {}
		self.version_provider: VersionProvider = version_provider
		self.versions: Tuple[str, str] = versions
	
	def loop(self, weighted_candidates_anchors: Dict[SHA, List[AnchorUpdateCandidate]]) -> Dict[SHA, List[Anchor]]:
		""" Loop the interaction for all the candidates. """
		for old_anchor, candidates in weighted_candidates_anchors.items():
			self.current_anchor_sha = old_anchor
			self.current_anchor = self.anchor_manager.get_by_id(old_anchor)
			self.current_candidates = candidates
			self.ask_anchor()
		self.end()
		return self.results
	
	def ask_anchor(self):
		""" Ask the user if anchor is at the right place. """
		self.results[self.current_anchor_sha] = list()
		self._display_candidates()
		
		validation = False
		while not validation:
			action = input("> ").split(' ')
			if len(action) == 0 or action[0] == 'h':
				print("Select all the new anchors needed to replace the old anchor.")
				print("Use 'v' to validate your chooses, or 'd' to not update the anchor.")
				print("q: quit")
				print("i: print candidates")
				print("c DIGIT: choose a candidate")
				print("n: create an other anchor")
				print("d: do not update the anchor. The annotations lost the link with the file.")
				print("v: validate the selection")
			elif action[0] == 'q':
				print("Quitting")
				raise InterruptedError("quitting")
			elif action[0] == 'i':
				self._display_candidates()  # verbose mode
			elif action[0] == 'c':
				if len(action) == 1:
					print("The candidate number is missing !")
				for selection in action[1:]:
					if not selection.isdigit() or int(selection) < 0 or int(selection) >= len(self.current_candidates):
						print(f"'{selection}' is not a valid candidate number.")
					else:
						self.results[self.current_anchor_sha].append(self.current_candidates[int(selection)][0])
			elif action[0] == 'n':
				try:
					self.new()
				except InterruptedError:
					print("Cancelled.")
			elif action[0] == "d":
				self.results[self.current_anchor_sha].clear()
				print(f"Anchor {self.current_anchor_sha[:16].hex()} is not updated.")
				validation = True
			elif action[0] == "v":
				if len(self.results[self.current_anchor_sha]) == 0:
					print("You should select at least 1 candidates, or use 'd'.")
				else:
					validation = True
	
	def _display_candidates(self):
		print(f"Anchor {self.current_anchor_sha.hex()}")
		print(f"in file '{self.current_anchor.filename}' from "
			f"({self.current_anchor.lines[0] + 1}:{self.current_anchor.line_offsets[0]}) "
			f"to ({self.current_anchor.lines[1] + 1}:{self.current_anchor.line_offsets[1]})")
		if self.current_candidates and len(self.current_candidates) > 0:
			print("{:^4} | {:^10} | {:^15}".format("num", "confidence", "lines"))
			print("-" * 40)
			for candidate_number, candidate in enumerate(self.current_candidates):
				n = str(candidate_number) + ('*' if candidate.anchor in self.results[self.current_anchor_sha] else ' ')
				if candidate.is_deletion():
					print(f"{n:^4} | {round(candidate.confidence, 2):^10} | DELETION")
				else:
					print(f"{n:^4} | {round(candidate.confidence, 2):^10} | {candidate.anchor.lines[0] + 1:>4}:"
						f"{candidate.anchor.line_offsets[0]:<3} -> {candidate.anchor.lines[1] + 1:>4}:"
						f"{candidate.anchor.line_offsets[1]:<3}")
		else:
			print("No candidates.")
	
	def display_anchor_context_verbose(self):
		""" Display the old anchor context in verbose mode. """
		anchor = self.current_anchor
		print("======= Anchor was BEFORE =======")
		print(f"------- In {anchor.filename} -------")
		labeled_anchor = {anchor: self.current_anchor_sha[:16].hex()}
		print(FAINT, end="")
		display.cat_anchors(labeled_anchor, anchor.filename, self.version_provider, self.versions[0])
		print(RESET, end="")
	
	def display_candidates_verbose(self):
		""" Display each candidat side by side with file content.
		It's verbose mode. """
		
		# Sort candidates by files
		candidates_by_file = {}
		
		for anchor, coeff in self.current_candidates:
			if anchor.filename not in candidates_by_file:
				# Create new anchor sublist
				candidates_by_file[anchor.filename] = [(anchor, coeff)]
			else:
				# Add to the correct one
				candidates_by_file[anchor.filename] += [(anchor, coeff)]
		
		print("======= Candidates are =======")
		for filename, candidates in candidates_by_file.items():
			print(f"------- In {filename} -------")
			for candidate_number, (anchor, coef) in enumerate(candidates):
				labeled_anchor = {anchor: "{}({}%)".format(candidate_number, int(coef * 100))}
				print(FAINT, end="")
				display.cat_anchors(labeled_anchor, anchor.filename, self.version_provider, self.versions[1])
				print(RESET, end="")
	
	def end(self):
		""" Called when the interactive resolution is finished. """
		pass  # logging.info(  #     "Finished ! The results that would be saved are\n %s", self.results  # )
	
	def new(self):
		""" Add a new candidate manually. """
		filename = ask_filename(self.current_anchor.filename)
		if not filename:
			return False
		content = self.version_provider.file_content(self.versions[1], filename).read().split("\n")
		start = ask_line(content)
		if start is None:
			return False
		end = ask_line(content)
		if end is None:
			return False
		
		new_anchor = a.Anchor(self.versions[1], filename, (start[0], end[0]), (start[1], end[1]))
		self.current_candidates.append((new_anchor, 1))
		self.results[self.current_anchor_sha].append(new_anchor)


def ask_filename(default: str) -> Optional[str]:
	"""
	Ask the user for a filename, provide the current anchor as default filename.
	:return: None if the user cancels
	"""
	filename = None
	while filename is None:
		filename = input(f"{BOLD}Filename ('{default}'):{RESET} ")
		if len(filename) == 0:
			filename = default
		elif filename == "q":
			break
		elif not isfile(filename):
			filename = None
	return filename


def ask_line(content: List[str]) -> Optional[Tuple[int, int]]:
	""" Ask the user for a line and an offset. Display vary if the line is
	the first or the last line of the anchor. """
	# LINE NUMBER
	line_number = None
	# print(FAINT, end="")
	# for i, line in enumerate(content):
	# 	print(f"{i + 1}: {line}")
	# print(RESET, end="")
	while line_number is None:
		line_number = input(f"{BOLD}Line number ?{RESET} ")
		if line_number == "q":
			return None
		elif not line_number.isdigit():
			print(f"'{line_number}' not an integer !")
			line_number = None
		elif int(line_number) >= len(content) or int(line_number) < 0:
			print(f"{line_number:d} out of file !")
			line_number = None
	line_number = int(line_number) - 1
	
	# OFFSET
	offset = None
	line = content[line_number]
	for i in range(0, len(line), 5):
		print(f"{i:<5}", end="")
	print()
	for i in range(0, len(line), 5):
		print(f"{'|':<5}", end="")
	print()
	for i in range(0, len(line), 5):
		print(BOLD + line[i] + RESET + line[i + 1: i + 5], end="")
	print()
	while offset is None:
		offset = input(f"{BOLD}Offset ?{RESET} ")
		if offset == "q":
			return None
		elif not offset.isdigit() and offset[0] != "-1" and not offset[1:].isdigit():
			print(f"'{offset}' not a digit !")
			offset = None
		elif int(offset) <= -len(line) or int(offset) > len(line):
			print(f"{offset:s} out of the line !")
			offset = None
	
	offset = int(offset)
	if offset < 0:
		offset = len(line) - offset + 1
	
	return line_number, offset
