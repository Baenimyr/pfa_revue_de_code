# Copyright (C) 2019 Eloïse Brocas, Mert Ernez, Jérôme Faure, Raphaël Gilliot
# Lucas Henry, Yann Julienne, Benjamin Le Rohellec
#
# This file is part of coderev, a code review tool.
#
# coderev is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# coderev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with coderev.  If not, see <https://www.gnu.org/licenses/>.
import os

from .anchor import *


class Annotation:
	""" Coderev object which contains the data that the user want to add to a
	bloc of text.
	
	An annotation as a title, some tags like 'FIXME' or 'TODO' and sometimes a description.
	"""
	
	def __init__(self, title: str, description: str = None, tags: Iterable[str] = None, creation_time: int = None):
		"""
		:param title: a title is mandatory
		:param description: can give a complex description to the annotation
		:param tags: give some tags
		"""
		assert isinstance(title, str) and len(title) > 0
		
		self.id: Optional[bytes] = None
		self._anchors: Set[SHA] = set()
		self._creation_time: int = creation_time if creation_time is not None else int(time.time())
		self._title: str = title
		self.description: str = description
		self.tags = set(tags) if tags else set()
		self.open: bool = True
	
	@property
	def creation_time(self) -> int:
		return self._creation_time
	
	@property
	def title(self) -> str:
		return self._title
	
	def files(self, anchor_manager: AnchorManager) -> Set[str]:
		"""
		Files in which the annotation is present.
		:param anchor_manager: The AnchorManager
		"""
		files = set()
		for anchor_sha in self._anchors:
			anchor = anchor_manager.get_by_id(anchor_sha)
			if anchor:
				files.add(anchor.filename)
		return files
	
	def close(self):
		"""
		Set annotation status to closed state.
		"""
		self.open = False
	
	def edit(self, anchors: List[SHA] = None, title: str = None, description: str = None, tags: List[str] = None, ):
		""" Edit the annotation with informations provided. For each argument,
		if it is not None, it will replace the current annotation value. """
		if anchors:
			self._anchors = set(anchors)
		if title:
			self._title = title
			self.id = None
		if description:
			self.description = description
		if tags:
			self.tags = tags
	
	def add_tag(self, tag: str) -> None:
		"""
		Add a tag to the annotation tag list.
		"""
		self.tags.add(tag)
	
	def remove_tag(self, tag: str) -> None:
		"""
		Remove a tag fro the annotation tag list.
		"""
		self.tags.remove(tag)
	
	@property
	def anchors(self) -> Iterable[SHA]:
		return iter(self._anchors)
	
	def bind_anchor(self, sha: SHA):
		self._anchors.add(sha)
	
	def unbind_anchor(self, sha: SHA):
		self._anchors.remove(sha)
	
	def __repr__(self) -> str:
		tags = ", ".join(self.tags)
		shas = ", ".join(a_sha.hex() for a_sha in self.anchors)
		return f"Annotation #{self.id.hex() if self.id else 0} <{tags}> ({shas})"


class AnnotationManager:
	"""
	This manager register all knows annotations.
	It is the main interface to access and modify annotations. It's also responsible of saving and importing annotations.
	"""
	
	def __init__(self):
		self._annotations: Dict[SHA, Annotation] = dict()
		self._known_annotations: Set[SHA] = set()
		
		dir = path.join(root_dir(), ".coderev", "annotations")
		if path.exists(dir):
			for file in os.listdir(dir):
				if file.endswith(".json"):
					sha = bytes.fromhex(file[:-5])
					assert len(sha) == 32
					self._known_annotations.add(sha)
	
	def get(self, i: SHA) -> Optional[Annotation]:
		"""
		:return: the annotation associated with this id
		"""
		if i in self._annotations.keys():
			return self._annotations.get(i)
		elif i in self._known_annotations:
			self.load_annotation(i)
			return self._annotations.get(i)
		else:
			return None
	
	def __contains__(self, item) -> bool:
		return item in self._known_annotations
	
	def get_ids(self) -> Iterable[SHA]:
		""":return: the list of all knows annotation's ids."""
		return self._known_annotations
	
	@staticmethod
	def _compute_annotation_sha(annotation: Annotation) -> SHA:
		sha = hashlib.sha256()
		sha.update(annotation.title.encode("utf8"))
		sha.update(annotation.creation_time.to_bytes(8, 'big', signed=False))
		return sha.digest()
	
	def expands_sha(self, sha: str) -> Optional[SHA]:
		"""
		Search among all knows SHA to expand this extract.
		:return: a complete SHA-256, starting with the value, or None if not found.
		"""
		for a_sha in self._known_annotations:
			if a_sha.hex().startswith(sha):
				return a_sha
		return None
	
	def register_annotation(self, annotation: Annotation) -> SHA:
		"""
		Register a new annotation.
		The annotation will now be saved on the disk and share with other users.
		"""
		annotation_sha = self._compute_annotation_sha(annotation)
		assert annotation_sha not in self._annotations
		self._annotations[annotation_sha] = annotation
		self._known_annotations.add(annotation_sha)
		annotation.id = annotation_sha
		return annotation_sha
	
	def delete_annotation(self, sha: SHA):
		"""
		Delete an annotation.
		The corresponding file is deleted on the filesystem.
		"""
		del self._annotations[sha]
		self._known_annotations.remove(sha)
		filename = path.join(root_dir(), ".coderev", "annotations", sha.hex() + ".json")
		if os.path.exists(filename):
			os.remove(filename)
	
	def list_anchors(self) -> Iterable[SHA]:
		"""
		Iter over all used anchor SHAs.
		
		This function is a generator. If a SHA is not in this list, the corresponding anchor is never used.
		"""
		known = set()
		self.load_all()
		for annotation in self._annotations.values():
			for anchor_sha in annotation.anchors:
				if anchor_sha not in known:
					known.add(anchor_sha)
					yield anchor_sha
	
	def get_annotations_for_anchor(self, anchor: SHA) -> Iterable[SHA]:
		"""
		Return the annotation that has `anchor`.
		:param anchor: id of the anchor concerned.
		:return: the annotation linked with this anchor, or None
		"""
		for annotation_sha in self._known_annotations:
			self.load_annotation(annotation_sha)
			if anchor in self._annotations[annotation_sha].anchors:
				yield annotation_sha
	
	def replace_anchor(self, old_anchor: SHA, new_anchor: SHA):
		"""
		Replace all occurrences of the old anchor by the new anchor.
		
		Annotations only store anchor SHAs they are bind with, but an anchor cannot be modified. To modify an anchor,
		the user must create a new anchor (with a different sha). This function binds the new anchor to all annotations
		bound with the old anchor, and unbinds the old anchor.
		"""
		self.load_all()
		for sha, annotation in self._annotations.items():
			if old_anchor in annotation.anchors:
				annotation.unbind_anchor(old_anchor)
				annotation.bind_anchor(new_anchor)
				self.save_annotation(sha)
	
	def unbind_anchor(self, anchor_sha: SHA):
		"""
		Remove all occurrences of the anchor in the annotations.
		"""
		self.load_all()
		for sha, annotation in self._annotations.items():
			if anchor_sha in annotation.anchors:
				annotation.unbind_anchor(anchor_sha)
				self.save_annotation(sha)
	
	def save_annotation(self, sha: SHA):
		annotation = self.get(sha)
		assert annotation is not None
		filename = path.join(root_dir(), ".coderev", "annotations", sha.hex() + ".json")
		if not path.exists(path.dirname(filename)):
			makedirs(path.dirname(filename))
		
		data = {"title": annotation.title, "desc": annotation.description, "tags": list(annotation.tags),
			"ctime": annotation.creation_time, "anchors": list(a.hex() for a in annotation.anchors)}
		with open(filename, "w", encoding="utf8") as file:
			json.dump(data, file, indent="\t", sort_keys=False, ensure_ascii=False)
	
	def save_all(self):
		for sha in self._annotations:
			self.save_annotation(sha)
	
	def load_annotation(self, sha: SHA):
		if sha in self._annotations:
			return
		
		filename = path.join(root_dir(), ".coderev", "annotations", sha.hex() + ".json")
		if path.exists(filename):
			with open(filename, "r", encoding="utf8") as file:
				data = json.load(file)
				annotation = Annotation(data["title"], data["desc"], data["tags"], data["ctime"])
				for anchor_sha in data["anchors"]:
					annotation.bind_anchor(bytes.fromhex(anchor_sha))
				assert self._compute_annotation_sha(annotation) == sha
				self.register_annotation(annotation)
	
	def load_all(self):
		for sha in self._known_annotations:
			if sha not in self._annotations:
				self.load_annotation(sha)


def garbage_collect(annotation_manager: AnnotationManager, anchor_manager: AnchorManager):
	"""
	Find and destroy all unused anchors. Don't use this function too quickly.
	
	An anchor not bound to an annotation is useless, except if it is about to be bind.
	"""
	annotation_manager.load_all()
	used_anchors = list(annotation_manager.list_anchors())
	unused_anchors = list()
	for anchor_sha in anchor_manager.anchors:
		if anchor_sha not in used_anchors:
			unused_anchors.append(anchor_sha)
	
	for anchor_sha in unused_anchors:
		anchor_manager.remove_anchor(anchor_sha)
