# Coderev

__Coderev__ is a tool to create non-intrusive comments.
These comments can be extended to conversation related to some part of the code.

While developping a project, many code parts can be quickly produced. In a collaborative environement, putting all those parts together in a coherent manner represents a significant work.
Developpers need to inspect the code produced by their co-workers in a process call code revue. The goal of this project is to simplify this task by providing tool allowing efficient discussion about the code.

This tool needs a version manager to work with.
For the time being, only git is supported.

## Install
Install from source with `make install` or `pip3 install .`.

Init the project directory with `coderev init` at the project root (near *.git*) or inside a subdirectory.

#### Requirements
Every requirement can be found in the file `requirements.txt`.
```shell script
pip3 install -r requirements.txt
```

## Usage
### Anchors
Anchors are data structures to select a precise continuous code area: a file, a character to start and a character to end.
An anchors cannot be moved and must be replaced.

#### Create a new anchor
`coderev.py add --anchor FILE LINE[:OFFSET] [LINE_END[:OFFSET]]`

  - FILE and LINE are used to locate the part of the code concerned by the comment
  - If present, can be use to create a new annotation.

#### Display anchors
`coderev.py display anchor [--all] [ID]`

  - By default, load only the current version anchors, use `--version COMMIT` to see anchors of an other version.
  - Anchors can be filtered by file, or by file and line.
  
#### Update
Anchors aim to follow the code they highlight.
An automatic process can generate anchors in the next commit if the corresponding code is not too mush altered.
The accepted modification coefficient is configured by *diff.ratio.hunk* and *diff.ratio.levenstein*.

The help of the user can be required. If not available, a copy of the anchor is created with the state __ERROR__ and
will not be updated in the next commits.

`coderev update` start the updating process.

### Annotation
#### Create a new annotation
`coderev.py add --annotation`

  - A title is mandatory.
  - You can add tags, a description and anchors id right now. Tags can be any short string, like _TEST_, _FIXME_, _ERROR_, ...

#### Display annotations
`coderev.py display annotation [-all] [ID] [--verbose]`

  - annotations can be filtered by id or file where present.
  - anchors used to filter or to display are loaded from the current version, or for all version in the parameter `--version`.

Display the file content and annotations together : `coderev.py cat file`

#### Edit an annotation
`coderev.py edit annotation ID`

With this command you can change the tag list (`--tags TODO ^FIXME` add the `TODO` tag and remove the `FIXME` tag)
and the description.
It is also possible to remove the annotation, but the corresponding anchors, even orphans, will not be deleted.

To change the anchors associated with the annotation, use `bind ANNOTATION ANCHOR` or `unbind ANNOTATION ANCHOR`.
The anchors must be present or created before.

## Authors
Current maintainer: Benjamin Le Rohellec

Original code by:
- Eloïse Brocas
- Mert Ernez
- Jérôme Faure
- Raphaël Gilliot
- Lucas Henry
- Yann Julienne
- Benjamin Le Rohellec

## License

coderev is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version.

coderev is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with coderev. If not, see <https://www.gnu.org/licenses/>.
